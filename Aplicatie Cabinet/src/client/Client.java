package client;
import java.io.IOException;
import java.net.*;

import model.Comenzi;
import view.LoginGui;

import java.io.*; 
  
public class Client
{
	static Socket socket;
    static OutputStream outputStream;
	static InputStream inputStream;
    static ObjectOutputStream outObj;
	static ObjectInputStream inObj;
	DataOutputStream dataOutputStream;
	DataInputStream dataInputStream;

	public static void comunica(Comenzi a)
	{
		try {
			
			outputStream = socket.getOutputStream();
			outObj = new ObjectOutputStream(outputStream);
			outObj.writeObject(a);
			outObj.flush();
			System.out.println("trimis!");
			
			inputStream = socket.getInputStream();
			inObj = new ObjectInputStream(inputStream);
			System.out.println("primit!");
			Comenzi rez = (Comenzi) inObj.readObject();
			ComenziMonitor.setComanda(rez.getComanda());
			ComenziMonitor.setObiecte(rez.getObiecte());
			
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) 
	{
		new LoginGui();
		
		try {
			socket = new Socket("127.0.0.1", 6868);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("Connected");
		
		while(true)
		{
		
		String var = ComenziMonitor.getComanda();
		
    	switch(var)
    	{
    	
    	case "verifica user": // cazul pentru logare, returneaza tipul userului daca acesta exista
    	{	
    		ComenziMonitor.setComanda(" ");
    		Comenzi a = new Comenzi("verifica user",ComenziMonitor.getObiecte());
    		comunica(a);
    		break;
    	}
    	
    	case "inregistrare":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("inregistrare",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "tabel secretari":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("tabel secretari",null);
    		comunica(b);
    		break;
    	}
    	
    	case "tabel dieteticieni":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("tabel dieteticieni",null);
    		comunica(b);
    		break;
    	}
    	
    	case "sterge secretar":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("sterge secretar",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "sterge dietetician":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("sterge dietetician",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "update secretar":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("update secretar",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "update dietetician":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("update dietetician",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "tabel clienti":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("tabel clienti",null);
    		comunica(b);
    		break;
    	}
    	
    	case "tabel programari":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("tabel programari",null);
    		comunica(b);
    		break;
    	}
    	
    	case "insert client":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("insert client",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "sterge client":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("sterge client",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "edit client":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("edit client",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "seteaza program":
    	{
    		
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("seteaza program",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "programeaza":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("programeaza",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "stergeProgramare":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("stergeProgramare",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "rapoarte":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("rapoarte",null);
    		comunica(b);
    		break;
    	}
    	
    	case "cautare nume":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("cautare nume",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "filtrareDupaDietetician":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("filtrareDupaDietetician",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "filtrare in functie de dieta":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("filtrare in functie de dieta",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "filtrare in functie de varsta":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("filtrare in functie de varsta",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "filtrare in functie de greutate":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("filtrare in functie de greutate",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "filtrareDupaDieteticianProgramari":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("filtrareDupaDieteticianProgramari",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "filtrareDupaVarsta":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("filtrareDupaVarsta",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "cautare nume dietetician":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("cautare nume dietetician",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "setare dieta":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("setare dieta",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "filtrareDupaGreutate":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("filtrareDupaGreutate",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	case "filtrareDupaDieta":
    	{
    		ComenziMonitor.setComanda(" ");
    		Comenzi b = new Comenzi("filtrareDupaDieta",ComenziMonitor.getObiecte());
    		comunica(b);
    		break;
    	}
    	
    	default :
    	{
    		break;
    	}
    	
	    }
    	
		}
	}
		
} 