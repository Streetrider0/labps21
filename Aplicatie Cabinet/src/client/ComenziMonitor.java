package client;

import java.util.ArrayList;

public class ComenziMonitor {

	private static volatile String comanda = "";
	private static volatile ArrayList<Object> obiecte = new ArrayList<Object>();
	
	public ComenziMonitor(String c,ArrayList<Object> a)
	{
		setComanda(c);
		setObiecte(a);
	}

	public static String getComanda() {
		return comanda;
	}

	public static void setComanda(String comanda) {
		ComenziMonitor.comanda = comanda;
	}

	public static ArrayList<Object> getObiecte() {
		return obiecte;
	}

	public static void setObiecte(ArrayList<Object> obiecte)
	{
		ComenziMonitor.obiecte = obiecte;
	}
}
