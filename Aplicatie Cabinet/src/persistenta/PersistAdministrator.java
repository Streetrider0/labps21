package persistenta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Administrator;

public class PersistAdministrator {

protected static final Logger LOGGER = Logger.getLogger(PersistAdministrator.class.getName());
	
	private final static String select="SELECT * FROM administrator";
	private final static String selectAdministrator="SELECT nume,email,parola,telefon FROM administrator WHERE idadministrator=?";
	private static final String insertAdministrator= "INSERT INTO administrator (nume,email,parola,telefon)"
			+ " VALUES (?,?,?,?)";
	private final static String deleteAdministrator="DELETE FROM administrator WHERE idadministrator=?";
	private final static String updateAdministrator="UPDATE administrator SET nume =?,email = ?,parola=?,telefon=? WHERE idadministrator=?"; 
	
	public static List<Administrator> administratori()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		List<Administrator> c = new ArrayList<Administrator>();
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(select);
			rs=selectAll.executeQuery();
			while(rs.next()) {
				int id=rs.getInt("idadministrator");
				String nume = rs.getString("nume");
				String email= rs.getString("email");
				String parola = rs.getString("parola");
				String telefon =rs.getString("telefon");
				c.add(new Administrator(id,nume,email,parola,telefon));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la actualizare tabel: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static Administrator select(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Administrator c=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectAdministrator);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			if(rs.next()) {
				String nume = rs.getString("nume");
				String email = rs.getString("email");
				String parola = rs.getString("parola");
				String telefon = rs.getString("telefon");
				c=new Administrator(id,nume,email,parola,telefon);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare Administrator: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int insert(Administrator c) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement insertStatement = null;
		ResultSet rs=null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertAdministrator, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, c.getNume());
			insertStatement.setString(2, c.getEmail());
			insertStatement.setString(3, c.getParola());
			insertStatement.setString(4, c.getTelefon());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la introducere administrator: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(insertStatement);
			ConnectionDB.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteAdministrator, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			int val=deleteStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost sters administratorul cu id "+id);
			}
			else
			{
				System.out.println("Nu exista administratorul cu id-ul "+id);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la stergere administrator:  " + e.getMessage());
		} finally {
			ConnectionDB.close(deleteStatement);
			ConnectionDB.close(dbConnection);
		}
	}
	
	public static void update(int id,String nume, String email,String parola,String telefon) 
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateAdministrator, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, nume);
			updateStatement.setString(2, email);
			updateStatement.setString(3, parola);
			updateStatement.setString(4, telefon);
			int val=updateStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost actualizat administratorul cu id "+id);
			}
			else
			{
				System.out.println("Administratorul cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la updatare administrator:  " + e.getMessage());
		} finally {
			ConnectionDB.close(updateStatement);
			ConnectionDB.close(dbConnection);
		}
	}
}
