package persistenta;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Clienti;


public class PersistClienti {

protected static final Logger LOGGER = Logger.getLogger(PersistClienti.class.getName());
	
	private final static String select="SELECT * FROM clienti";
	private final static String selectClient="SELECT idclient,nume,varsta,greutate,dieta,telefon FROM clienti WHERE nume=?";
	private final static String filtrareDieta="SELECT idclient FROM clienti WHERE dieta=?";
	private final static String filtrareVarsta="SELECT idclient FROM clienti WHERE varsta=?";
	private final static String filtrareGreutate="SELECT idclient FROM clienti WHERE greutate=?";
	private final static String selectClientId="SELECT idclient,nume,varsta,greutate,dieta,telefon FROM clienti WHERE idclient=?";

	private static final String insertClient= "INSERT INTO clienti (nume,varsta,greutate,dieta,telefon)"
			+ " VALUES (?,?,?,?,?)";
	private final static String deleteClient="DELETE FROM clienti WHERE idclient=?";
	private final static String updateClient="UPDATE clienti SET nume =?,varsta = ?,greutate=?,dieta=?,telefon=? WHERE idclient=?"; 
	
	public static List<Clienti> clienti()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		List<Clienti> c = new ArrayList<Clienti>();
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(select);
			rs=selectAll.executeQuery();
			while(rs.next()) {
				int id=rs.getInt("idclient");
				String nume = rs.getString("nume");
				int varsta = rs.getInt("varsta");
				int greutate = rs.getInt("greutate");
				String dieta = rs.getString("dieta");
				String telefon =rs.getString("telefon");
				c.add(new Clienti(id,nume,varsta,greutate,dieta,telefon));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la actualizare tabel: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static Clienti select(String numec)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Clienti c=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectClient);
			selectStatement.setString(1, numec);
			rs=selectStatement.executeQuery();
			if(rs.next()) {
				int id=rs.getInt("idclient");
				String nume = rs.getString("nume");
				int varsta = rs.getInt("varsta");
				int greutate = rs.getInt("greutate");
				String dieta = rs.getString("dieta");
				String telefon = rs.getString("telefon");
				c=new Clienti(id,nume,varsta,greutate,dieta,telefon);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare client: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static ArrayList<Object> filtrareDupaDieta(String dieta)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement filtreaza = null;
		ArrayList<Object> c = new  ArrayList<Object>();
		ResultSet rs=null;
		try {
			filtreaza = dbConnection.prepareStatement(filtrareDieta);
			filtreaza.setString(1, dieta);
			rs=filtreaza.executeQuery();
			while(rs.next()) {
				int id_client = rs.getInt("idClient");
				c.add(id_client);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la filtrare dupe Dieta: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(filtreaza);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static ArrayList<Object> filtrareDupaVarsta(int varsta)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement filtreaza = null;
		ArrayList<Object> c = new  ArrayList<Object>();
		ResultSet rs=null;
		try {
			filtreaza = dbConnection.prepareStatement(filtrareVarsta);
			filtreaza.setInt(1, varsta);
			rs=filtreaza.executeQuery();
			while(rs.next()) {
				int id_client = rs.getInt("idClient");
				c.add(id_client);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la filtrare dupe Varsta: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(filtreaza);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static ArrayList<Object> filtrareDupaGreutate(int greutate)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement filtreaza = null;
		ArrayList<Object> c = new  ArrayList<Object>();
		ResultSet rs=null;
		try {
			filtreaza = dbConnection.prepareStatement(filtrareGreutate);
			filtreaza.setInt(1, greutate);
			rs=filtreaza.executeQuery();
			while(rs.next()) {
				int id_client = rs.getInt("idClient");
				c.add(id_client);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la filtrare dupe Greutate: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(filtreaza);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static Clienti selectId(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Clienti c=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectClientId);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			if(rs.next()) {
				int id1=rs.getInt("idclient");
				String nume = rs.getString("nume");
				int varsta = rs.getInt("varsta");
				int greutate = rs.getInt("greutate");
				String dieta = rs.getString("dieta");
				String telefon = rs.getString("telefon");
				c=new Clienti(id1,nume,varsta,greutate,dieta,telefon);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare client: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int insert(Clienti c) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement insertStatement = null;
		ResultSet rs=null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertClient, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, c.getNume());
			insertStatement.setInt(2, c.getVarsta());
			insertStatement.setInt(3, c.getGreutate());
			insertStatement.setString(4, c.getDieta());
			insertStatement.setString(5, c.getTelefon());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la introducere client: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(insertStatement);
			ConnectionDB.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteClient, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			int val=deleteStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost sters clientul cu id "+id);
			}
			else
			{
				System.out.println("Nu exista client cu id-ul "+id);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la stergere client:  " + e.getMessage());
		} finally {
			ConnectionDB.close(deleteStatement);
			ConnectionDB.close(dbConnection);
		}
	}
	
	public static void update(int id,String nume,int varsta,int greutate,String dieta,String telefon) 
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateClient, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, nume);
			updateStatement.setInt(2, varsta);
			updateStatement.setInt(3, greutate);
			updateStatement.setString(4, dieta);
			updateStatement.setString(5, telefon);
			updateStatement.setInt(6,id);
			int val=updateStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost actualizat clientul cu id "+id);
			}
			else
			{
				System.out.println("Clientul cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la updatare client:  " + e.getMessage());
		} finally {
			ConnectionDB.close(updateStatement);
			ConnectionDB.close(dbConnection);
		}
	}
}