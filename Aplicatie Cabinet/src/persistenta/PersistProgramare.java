package persistenta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Dietetician;
import model.Programare;

public class PersistProgramare {

protected static final Logger LOGGER = Logger.getLogger(PersistProgramare.class.getName());
	
	private final static String select="SELECT * FROM programari";
	private final static String searchNameClient="SELECT clienti.idclient FROM clienti INNER JOIN programari ON clienti.idclient=programari.idClient WHERE programari.iddietetician=? AND clienti.nume=?";
	private final static String selectProgramare="SELECT iddietetician,idClient,dataSiOra FROM programari WHERE idprogramari=?";
	private final static String selectProgramareDietet="SELECT idprogramari FROM programari WHERE iddietetician=?";
	private static final String insertProgramare= "INSERT INTO programari (iddietetician,idClient,dataSiOra)"
			+ " VALUES (?,?,?)";
	private final static String deleteProgramare="DELETE FROM programari WHERE idprogramari=?";
	private final static String searchDietetDate="SELECT COUNT(idprogramari) FROM programari WHERE iddietetician = ? AND dataSiOra = ?;";
	private final static String filtrareDietetician="SELECT DISTINCT idClient FROM programari WHERE iddietetician=?";
	private final static String filtrareDupaDieta="SELECT clienti.idclient FROM clienti INNER JOIN programari ON clienti.idclient=programari.idClient WHERE programari.iddietetician=? AND clienti.dieta=?";
	private final static String filtrareDupaVarsta="SELECT clienti.idclient FROM clienti INNER JOIN programari ON clienti.idclient=programari.idClient WHERE programari.iddietetician=? AND clienti.varsta=?";
	private final static String filtrareDupaGreutate="SELECT clienti.idclient FROM clienti INNER JOIN programari ON clienti.idclient=programari.idClient WHERE programari.iddietetician=? AND clienti.greutate=?";

	public static List<Programare> programari()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		List<Programare> c = new ArrayList<Programare>();
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(select);
			rs=selectAll.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("idprogramari");
				int dietet = rs.getInt("iddietetician");
				int idClient = rs.getInt("idClient");
				String data = rs.getString("dataSiOra");
				c.add(new Programare(id,dietet,idClient,data));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la actualizare tabel: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static ArrayList<Object> serchClient(int dietetician,String nume)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		ArrayList<Object> c = new ArrayList<Object>();
		
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(searchNameClient);
			selectAll.setInt(1, dietetician);
			selectAll.setString(2, nume);
			rs=selectAll.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("idClient");
				c.add(id);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare client: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static Programare select(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Programare c=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectProgramare);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			if(rs.next()) {
				int dietet = rs.getInt("iddietetician");
				int client = rs.getInt("idClient");
				String data = rs.getString("dataSiOra");
				c=new Programare(id,dietet,client,data);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare programare: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static ArrayList<Object> selectProgramareDietetician(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		ArrayList<Object> c = new  ArrayList<Object>();
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectProgramareDietet);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			while(rs.next()) {
				int dietet = rs.getInt("idprogramari");
				c.add(dietet);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare programare: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static ArrayList<Object> filtrareDupaDieta(int id,String dieta)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		ArrayList<Object> c = new  ArrayList<Object>();
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(filtrareDupaDieta);
			selectStatement.setInt(1, id);
			selectStatement.setString(2, dieta);
			rs=selectStatement.executeQuery();
			while(rs.next()) {
				int dietet = rs.getInt("idclient");
				c.add(dietet);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare clienti dupa dieta: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int insert(Programare c) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement insertStatement = null;
		ResultSet rs=null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertProgramare, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, c.getId_dietet());
			insertStatement.setInt(2, c.getId_client());
			insertStatement.setString(3, c.getData_ora());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la introducere programare: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(insertStatement);
			ConnectionDB.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteProgramare, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			int val=deleteStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost stearsa programarea cu id "+id);
			}
			else
			{
				System.out.println("Nu exista programarea cu id-ul "+id);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la stergere dietetician:  " + e.getMessage());
		} finally {
			ConnectionDB.close(deleteStatement);
			ConnectionDB.close(dbConnection);
		}
	}
	
	public static int verifValiditate(int dietet,String data)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Programare c=null;
		PreparedStatement count = null;
		ResultSet rs=null;
		int count1 = 0;
		try {
			count = dbConnection.prepareStatement(searchDietetDate);
			count.setInt(1, dietet);
			count.setString(2, data);
			rs=count.executeQuery();
			if(rs.next()) {
				count1 = rs.getInt("COUNT(idprogramari)");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la validare programare: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(count);
			ConnectionDB.close(dbConnection);
		}
		return count1;
	}
	
	public static ArrayList<Object> filtrareDupaDietetetician(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement filtreaza = null;
		ArrayList<Object> c = new  ArrayList<Object>();
		ResultSet rs=null;
		try {
			filtreaza = dbConnection.prepareStatement(filtrareDietetician);
			filtreaza.setInt(1, id);
			rs=filtreaza.executeQuery();
			while(rs.next()) {
				int id_client = rs.getInt("idClient");
				c.add(id_client);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la filtrare dupe Dietetician: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(filtreaza);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}

	public static ArrayList<Object> filtrareDupaVarsta(int id, int varsta) {
		Connection dbConnection = ConnectionDB.getConnection();
		ArrayList<Object> c = new  ArrayList<Object>();
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(filtrareDupaVarsta);
			selectStatement.setInt(1, id);
			selectStatement.setInt(2, varsta);
			rs=selectStatement.executeQuery();
			while(rs.next()) {
				int dietet = rs.getInt("idclient");
				c.add(dietet);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare clienti dupa varsta: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static ArrayList<Object> filtrareDupaGreutate(int id, int greutate) {
		Connection dbConnection = ConnectionDB.getConnection();
		ArrayList<Object> c = new  ArrayList<Object>();
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(filtrareDupaGreutate);
			selectStatement.setInt(1, id);
			selectStatement.setInt(2, greutate);
			rs=selectStatement.executeQuery();
			while(rs.next()) {
				int dietet = rs.getInt("idclient");
				c.add(dietet);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare clienti dupa greutate: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
}
