package persistenta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Dietetician;

public class PersistDietetician {

protected static final Logger LOGGER = Logger.getLogger(PersistDietetician.class.getName());
	
	private final static String select="SELECT * FROM dietetician";
	private final static String selectDietetician="SELECT nume,email,parola,program,telefon FROM dietetician WHERE iddietetician=?";
	private static final String insertDietetician= "INSERT INTO dietetician (nume,email,parola,program,telefon)"
			+ " VALUES (?,?,?,?,?)";
	private final static String deleteDietetician="DELETE FROM dietetician WHERE iddietetician=?";
	private final static String updateDietetician="UPDATE dietetician SET nume =?,email = ?, program=?,telefon=? WHERE iddietetician=?"; 
	
	public static List<Dietetician> dieteticieni()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		List<Dietetician> c = new ArrayList<Dietetician>();
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(select);
			rs=selectAll.executeQuery();
			while(rs.next()) {
				int id = rs.getInt("idDietetician");
				String nume = rs.getString("nume");
				String email= rs.getString("email");
				String parola = rs.getString("parola");
				String program = rs.getString("program");
				String telefon = rs.getString("telefon");
				c.add(new Dietetician(id,nume,email,parola,program,telefon));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la actualizare tabel: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static Dietetician select(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Dietetician c=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectDietetician);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			if(rs.next()) {
				String nume = rs.getString("nume");
				String email = rs.getString("email");
				String parola = rs.getString("parola");
				String program= rs.getString("program");
				String telefon = rs.getString("telefon");
				c=new Dietetician(id,nume,email,parola,program,telefon);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare dietetician: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int insert(Dietetician c) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement insertStatement = null;
		ResultSet rs=null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertDietetician, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, c.getNume());
			insertStatement.setString(2, c.getEmail());
			insertStatement.setString(3, c.getParola());
			insertStatement.setString(4, c.getProgram());
			insertStatement.setString(5, c.getTelefon());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la introducere dietetician: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(insertStatement);
			ConnectionDB.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteDietetician, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			int val=deleteStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost sters dieteticianul cu id "+id);
			}
			else
			{
				System.out.println("Nu exista dieteticianul cu id-ul "+id);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la stergere dietetician:  " + e.getMessage());
		} finally {
			ConnectionDB.close(deleteStatement);
			ConnectionDB.close(dbConnection);
		}
	}
	
	public static void update(int id,String nume, String email,String program,String telefon) 
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateDietetician, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, nume);
			updateStatement.setString(2, email);
			updateStatement.setString(3, program);
			updateStatement.setString(4, telefon);
			updateStatement.setInt(5, id);
			int val=updateStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost actualizat dieteticianul cu id "+id);
			}
			else
			{
				System.out.println("dieteticianul cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la updatare dietetician:  " + e.getMessage());
		} finally {
			ConnectionDB.close(updateStatement);
			ConnectionDB.close(dbConnection);
		}
	}
}
