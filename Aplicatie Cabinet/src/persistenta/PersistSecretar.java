package persistenta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Secretar;

public class PersistSecretar {

protected static final Logger LOGGER = Logger.getLogger(PersistSecretar.class.getName());
	
	private final static String select="SELECT * FROM secretar";
	private final static String selectSecretar="SELECT nume,email,parola,telefon FROM secretar WHERE idsecretar=?";
	private static final String insertSecretar= "INSERT INTO secretar (nume,email,parola,telefon)"
			+ " VALUES (?,?,?,?)";
	private final static String deleteSecretar="DELETE FROM secretar WHERE idsecretar=?";
	private final static String updateSecretar="UPDATE secretar SET nume =?,email = ?,telefon=? WHERE idsecretar=?"; 
	
	public static List<Secretar> secretari()
	{
		Connection dbConnection = ConnectionDB.getConnection();
		PreparedStatement selectAll = null;
		List<Secretar> c = new ArrayList<Secretar>();
		ResultSet rs=null;
		try {
			selectAll = dbConnection.prepareStatement(select);
			rs=selectAll.executeQuery();
			while(rs.next()) {
				int id=rs.getInt("idSecretar");
				String nume = rs.getString("nume");
				String email= rs.getString("email");
				String parola = rs.getString("parola");
				String telefon = rs.getString("telefon");
				c.add(new Secretar(id,nume,email,parola,telefon));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la actualizare tabel: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectAll);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static Secretar select(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();
		Secretar c=null;
		PreparedStatement selectStatement = null;
		ResultSet rs=null;
		try {
			selectStatement = dbConnection.prepareStatement(selectSecretar);
			selectStatement.setInt(1, id);
			rs=selectStatement.executeQuery();
			if(rs.next()) {
				String nume = rs.getString("nume");
				String email = rs.getString("email");
				String parola = rs.getString("parola");
				String telefon = rs.getString("telefon");
				c=new Secretar(id,nume,email,parola,telefon);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la cautare secretar: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(selectStatement);
			ConnectionDB.close(dbConnection);
		}
		return c;
	}
	
	public static int insert(Secretar c) {
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement insertStatement = null;
		ResultSet rs=null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertSecretar, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, c.getNume());
			insertStatement.setString(2, c.getEmail());
			insertStatement.setString(3, c.getParola());
			insertStatement.setString(4, c.getTelefon());
			insertStatement.executeUpdate();

			rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la introducere secretar: " + e.getMessage());
		} finally {
			ConnectionDB.close(rs);
			ConnectionDB.close(insertStatement);
			ConnectionDB.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int id)
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteSecretar, Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setInt(1, id);
			int val=deleteStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost sters secretarul cu id "+id);
			}
			else
			{
				System.out.println("Nu exista secretarul cu id-ul "+id);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la stergere secretar:  " + e.getMessage());
		} finally {
			ConnectionDB.close(deleteStatement);
			ConnectionDB.close(dbConnection);
		}
	}
	
	public static void update(int id,String nume, String email,String telefon) 
	{
		Connection dbConnection = ConnectionDB.getConnection();

		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateSecretar, Statement.RETURN_GENERATED_KEYS);
			updateStatement.setString(1, nume);
			updateStatement.setString(2, email);
			updateStatement.setString(3, telefon);
			updateStatement.setInt(4, id);
			int val=updateStatement.executeUpdate();
			if(val!=0)
			{
			System.out.println("A fost actualizat secretarul cu id "+id);
			}
			else
			{
				System.out.println("Secretarul cu id "+id+" nu exista");
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Exceptie la updatare secretar:  " + e.getMessage());
		} finally {
			ConnectionDB.close(updateStatement);
			ConnectionDB.close(dbConnection);
		}
	}
}
