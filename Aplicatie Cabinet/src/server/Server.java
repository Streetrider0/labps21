package server;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import client.Client;
import client.ComenziMonitor;
import model.Comenzi;
import model.Dietetician;
import model.Programare;
import model.Secretar;
import model.Administrator;
import model.Clienti;
import model.User;
import persistenta.PersistAdministrator;
import persistenta.PersistClienti;
import persistenta.PersistDietetician;
import persistenta.PersistProgramare;
import persistenta.PersistSecretar;
import view.MesajGui;

import java.io.*; 
  
public class Server
{ 
	int flagServer=0;
	ServerSocket server;
	Socket socket;
	InputStream input;
	OutputStream out;
	ObjectInputStream objectInputStream;
	ObjectOutputStream objectOutputStream;
	DataOutputStream dataOutputStream;
	DataInputStream dataInputStream;
	Comenzi in;
    public Server()  throws IOException, ClassNotFoundException
    { 
		server = new ServerSocket(6868);
    	System.out.println("Serverul a pornit.");
    	
    	socket = server.accept();
    	System.out.println("Server conectat.");
    	
    	while(true)
    	{
    	
    	input = socket.getInputStream();
    	objectInputStream = new ObjectInputStream(input);
    	in = (Comenzi)objectInputStream.readObject();
    	
    	switch(in.getComanda())
    	{
    	
    	case "verifica user": 
    	{
    		ArrayList<Object> useri = new ArrayList<Object>();
    		for(Administrator a : PersistAdministrator.administratori())
    		{
    			useri.add((Object)a);
    		}
    		for(Dietetician a :PersistDietetician.dieteticieni())
    		{
    			useri.add((Object)a);
    		}
    		for(Secretar a :PersistSecretar.secretari())
    		{
    			useri.add((Object)a);
    		}
 
			Object found = useri.stream().filter(x -> ((User) in.getObiecte().get(0)).getEmail().equals(((User) x).getEmail()) && ((User) in.getObiecte().get(0)).getParola().equals(((User) x).getParola())).findAny().orElse(null);
			
			ArrayList<Object> utilizator =new ArrayList<Object>();
			Comenzi rez;
			if(found==null)
			{
				rez = new Comenzi("inexistent",null);
			}
			else
			{
				utilizator.add(found);
				rez = new Comenzi(found.getClass().toString(),utilizator);
			}
    		
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
   
    	
    	case "inregistrare":
    	{
    		
    		switch((String)in.getObiecte().get(0))
    		{
    		case "dietetician":
    			PersistDietetician.insert(new Dietetician(0,(String)in.getObiecte().get(1),(String)in.getObiecte().get(2),(String)in.getObiecte().get(3),null,(String)in.getObiecte().get(4)));
    			break;
    		case "secretar":
    			System.out.println("Aici!");
    			PersistSecretar.insert(new Secretar(0,(String)in.getObiecte().get(1),(String)in.getObiecte().get(2),(String)in.getObiecte().get(3),(String)in.getObiecte().get(4)));
    			break;
    		case "administrator":
    			PersistAdministrator.insert(new Administrator(0,(String)in.getObiecte().get(1),(String)in.getObiecte().get(2),(String)in.getObiecte().get(3),(String)in.getObiecte().get(4)));
    			break;
    		}
    		
    		Comenzi rez=new Comenzi("cont creat",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "tabel secretari":
    	{
    		ArrayList<Secretar> secretari = (ArrayList<Secretar>) PersistSecretar.secretari();
    		ArrayList<Object> send = new ArrayList<Object>();
    		for (int i = 0; i < secretari.size(); i++){

    			int id=((User) secretari.get(i)).getId();
    			String nume=((User) secretari.get(i)).getNume();
    			String email=((User) secretari.get(i)).getEmail();
    			String telefon=((User) secretari.get(i)).getTelefon();
    			send.add(id);
    			send.add(nume);
    			send.add(email);
    			send.add(telefon);
    		}
    		
    		Comenzi rez=new Comenzi("secretari",send);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "tabel dieteticieni":
    	{
    		ArrayList<Object> send = new ArrayList<Object>();
    		ArrayList<Dietetician> dieteticieni = (ArrayList<Dietetician>) PersistDietetician.dieteticieni();
    		for (int i = 0; i < dieteticieni.size(); i++){

    			int id=((User) dieteticieni.get(i)).getId();
    			String nume=((User) dieteticieni.get(i)).getNume();
    			String email=((User) dieteticieni.get(i)).getEmail();
    			String telefon=((User) dieteticieni.get(i)).getTelefon();
    			String program=((Dietetician)dieteticieni.get(i)).getProgram();
    			send.add(id);
    			send.add(nume);
    			send.add(email);
    			send.add(telefon);
    			send.add(program);
    		}
    		
    		System.out.println("dieteticieni");
    		Comenzi rez=new Comenzi("dieteticieni",send);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "tabel programari":
    	{
    		ArrayList<Programare> programari = (ArrayList<Programare>) PersistProgramare.programari();
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < programari.size(); i++){
    			int id=(programari.get(i)).getId();
    			int dietetician=(programari.get(i)).getId_dietet();
    			int client=(programari.get(i)).getId_client();
    			String data=(programari.get(i)).getData_ora();
    			send.add(id);
    			send.add(dietetician);
    			send.add(client);
    			send.add(data);
    		}
    		
    		Comenzi rez=new Comenzi("programari",send);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "insert client":
    	{
    		Clienti c =new Clienti(0,(String)in.getObiecte().get(0),(int) in.getObiecte().get(1),(int) in.getObiecte().get(2),null,(String)in.getObiecte().get(3));
    		PersistClienti.insert(c);
    		Comenzi rez=new Comenzi("inserat",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "tabel clienti":
    	{
    		ArrayList<Clienti> clienti = (ArrayList<Clienti>) PersistClienti.clienti();
    		ArrayList<Object> send = new ArrayList<Object>();
    		for (int i = 0; i < clienti.size(); i++){

    			int id=(clienti.get(i)).getId();
    			String nume=(clienti.get(i)).getNume();
    			int varsta=(clienti.get(i)).getVarsta();
    			int greutate=(clienti.get(i)).getGreutate();
    			String dieta=(clienti.get(i)).getDieta();
    			String telefon=(clienti.get(i)).getTelefon();
    			send.add(id);
    			send.add(nume);
    			send.add(varsta);
    			send.add(greutate);
    			if(dieta==null)
    			{
    			dieta=" ";
    			send.add(dieta);
    			}
    			else
    			{
    				send.add(dieta);
    			}
    			send.add(telefon);
    		}
    		
    		Comenzi rez=new Comenzi("clienti",send);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "cautare nume":
    	{
    		String nume = (String) in.getObiecte().get(0);
    		Clienti c=PersistClienti.select(nume);
    		Comenzi rez;
    		if(c==null)
    		{
    		rez=new Comenzi("negasit",null);
    		}
    		else
    		{
    		ArrayList<Object> clientt = new ArrayList<Object>();
    		clientt.add((int)c.getId());
    		clientt.add((String)c.getNume());
    		clientt.add((int)c.getVarsta());
    		clientt.add((int)c.getGreutate());
    		clientt.add((String)c.getDieta());
    		clientt.add((String)c.getTelefon());
    		rez=new Comenzi("gasit", clientt);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "filtrareDupaDietetician":
    	{
    		int id = (int) in.getObiecte().get(0);
    		ArrayList<Object> clienti = PersistProgramare.filtrareDupaDietetetician(id);
    		ArrayList<Clienti> clienti1 = new ArrayList<Clienti>();
    		for(Object o : clienti)
    		{
    			clienti1.add((Clienti) PersistClienti.selectId((int) o));
    		}
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < clienti1.size(); i++)
    		{
    			System.out.println(clienti1.get(i).getNume());
    			int id1=(clienti1.get(i)).getId();
    			String nume=(clienti1.get(i)).getNume();
    			int varsta=(clienti1.get(i)).getVarsta();
    			int greutate=(clienti1.get(i)).getGreutate();
    			String dieta=(clienti1.get(i)).getDieta();
    			String telefon=(clienti1.get(i)).getTelefon();
    			send.add(id1);
    			send.add(nume);
    			send.add(varsta);
    			send.add(greutate);
    			if(dieta==null)
    			{
    			dieta=" ";
    			send.add(dieta);
    			}
    			else
    			{
    				send.add(dieta);
    			}
    			send.add(telefon);
    		}
    		Comenzi rez;
    		if(clienti1.size()==0)
    		{
    			rez=new Comenzi("nefiltrat",null);
    		}
    		else
    		{
    		 rez=new Comenzi("filtrat",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "filtrareDupaDieteticianProgramari":
    	{
    		int id =  (int) in.getObiecte().get(0);
    		ArrayList<Object> programari = PersistProgramare.selectProgramareDietetician(id);
    		ArrayList<Programare> programari1 = new ArrayList<Programare>();
    		for(Object o : programari)
    		{
    			programari1.add((Programare) PersistProgramare.select((int) o));
    		}
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < programari1.size(); i++)
    		{
    			int id1=(programari1.get(i)).getId();
    			int dietetician=(programari1.get(i)).getId_dietet();
    			int client=(programari1.get(i)).getId_client();
    			String data=(programari1.get(i)).getData_ora();
    			send.add(id1);
    			send.add(dietetician);
    			send.add(client);
    			send.add(data);
    		}
    		Comenzi rez;
    		if(programari1.size()==0)
    		{
    			rez=new Comenzi("nefiltrat",null);
    		}
    		else
    		{
    		 rez=new Comenzi("filtrat",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "filtrareDupaDieta":
    	{
    		String dieta = (String) in.getObiecte().get(0);
    		ArrayList<Object> clienti = PersistClienti.filtrareDupaDieta(dieta);
    		ArrayList<Clienti> clienti1 = new ArrayList<Clienti>();
    		for(Object o : clienti)
    		{
    			clienti1.add((Clienti) PersistClienti.selectId((int) o));
    		}
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < clienti1.size(); i++)
    		{
    			System.out.println(clienti1.get(i).getNume());
    			int id1=(clienti1.get(i)).getId();
    			String nume=(clienti1.get(i)).getNume();
    			int varsta=(clienti1.get(i)).getVarsta();
    			int greutate=(clienti1.get(i)).getGreutate();
    			String dieta1=(clienti1.get(i)).getDieta();
    			String telefon=(clienti1.get(i)).getTelefon();
    			send.add(id1);
    			send.add(nume);
    			send.add(varsta);
    			send.add(greutate);
    			if(dieta1==null)
    			{
    			dieta1=" ";
    			send.add(dieta1);
    			}
    			else
    			{
    				send.add(dieta1);
    			}
    			send.add(telefon);
    		}
    		Comenzi rez;
    		if(clienti1.size()==0)
    		{
    			rez=new Comenzi("nefiltrat",null);
    		}
    		else
    		{
    		 rez=new Comenzi("filtrat",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		
    		break;
    	}
    	
    	case "filtrareDupaVarsta":
    	{
    		int varsta = Integer.parseInt((String) in.getObiecte().get(0));
    		ArrayList<Object> clienti = PersistClienti.filtrareDupaVarsta(varsta);
    		ArrayList<Clienti> clienti1 = new ArrayList<Clienti>();
    		for(Object o : clienti)
    		{
    			clienti1.add((Clienti) PersistClienti.selectId((int) o));
    		}
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < clienti1.size(); i++)
    		{
    			System.out.println(clienti1.get(i).getNume());
    			int id1=(clienti1.get(i)).getId();
    			String nume=(clienti1.get(i)).getNume();
    			int varsta1=(clienti1.get(i)).getVarsta();
    			int greutate=(clienti1.get(i)).getGreutate();
    			String dieta1=(clienti1.get(i)).getDieta();
    			String telefon=(clienti1.get(i)).getTelefon();
    			send.add(id1);
    			send.add(nume);
    			send.add(varsta1);
    			send.add(greutate);
    			if(dieta1==null)
    			{
    			dieta1=" ";
    			send.add(dieta1);
    			}
    			else
    			{
    				send.add(dieta1);
    			}
    			send.add(telefon);
    		}
    		Comenzi rez;
    		if(clienti1.size()==0)
    		{
    			rez=new Comenzi("nefiltrat",null);
    		}
    		else
    		{
    		 rez=new Comenzi("filtrat",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		
    		break;
    	}
    	
    	case "filtrareDupaGreutate":
    	{
    		int greutate = Integer.parseInt((String) in.getObiecte().get(0));
    		ArrayList<Object> clienti = PersistClienti.filtrareDupaGreutate(greutate);
    		ArrayList<Clienti> clienti1 = new ArrayList<Clienti>();
    		for(Object o : clienti)
    		{
    			clienti1.add((Clienti) PersistClienti.selectId((int) o));
    		}
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < clienti1.size(); i++)
    		{
    			System.out.println(clienti1.get(i).getNume());
    			int id1=(clienti1.get(i)).getId();
    			String nume=(clienti1.get(i)).getNume();
    			int varsta1=(clienti1.get(i)).getVarsta();
    			int greutate1=(clienti1.get(i)).getGreutate();
    			String dieta1=(clienti1.get(i)).getDieta();
    			String telefon=(clienti1.get(i)).getTelefon();
    			send.add(id1);
    			send.add(nume);
    			send.add(varsta1);
    			send.add(greutate1);
    			if(dieta1==null)
    			{
    			dieta1=" ";
    			send.add(dieta1);
    			}
    			else
    			{
    				send.add(dieta1);
    			}
    			send.add(telefon);
    		}
    		Comenzi rez;
    		if(clienti1.size()==0)
    		{
    			rez=new Comenzi("nefiltrat",null);
    		}
    		else
    		{
    		 rez=new Comenzi("filtrat",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		
    		break;
    	}
    	
    	case "programeaza":
    	{
    		int dietet=(int) in.getObiecte().get(0);
    		int client=(int) in.getObiecte().get(1);
    		String data=(String) in.getObiecte().get(2);
    		Dietetician d = PersistDietetician.select(dietet);
    		int rez=PersistProgramare.verifValiditate(dietet, data);
    		if(rez==0)
    		{
    			int ora = Character.getNumericValue(data.charAt(0))*10+Character.getNumericValue(data.charAt(1));
    			try {
    			if(ora>=Character.getNumericValue( d.getProgram().charAt(0))*10+Character.getNumericValue( d.getProgram().charAt(1)) && ora < Character.getNumericValue( d.getProgram().charAt(6))*10+Character.getNumericValue( d.getProgram().charAt(7)))
    			{
    				PersistProgramare.insert(new Programare(0,dietet,client,data));
    				Comenzi rez1=new Comenzi("programat",null);
            		out = socket.getOutputStream();
            		objectOutputStream = new ObjectOutputStream(out);
            		objectOutputStream.writeObject(rez1);
            		objectOutputStream.flush();
    			}
    			else
    			{
    				Comenzi rez1=new Comenzi("esuat",null);
            		out = socket.getOutputStream();
            		objectOutputStream = new ObjectOutputStream(out);
            		objectOutputStream.writeObject(rez1);
            		objectOutputStream.flush();
    			}
    			}catch(Exception NullPointerException)
    			{
    				new MesajGui("Dieteticianul nu are un program setat!");
    				Comenzi rez1=new Comenzi("esuat",null);
            		out = socket.getOutputStream();
            		objectOutputStream = new ObjectOutputStream(out);
            		objectOutputStream.writeObject(rez1);
            		objectOutputStream.flush();
    			}
    			
    		}
    		else
    		{
    			Comenzi rez1=new Comenzi("esuat",null);
        		out = socket.getOutputStream();
        		objectOutputStream = new ObjectOutputStream(out);
        		objectOutputStream.writeObject(rez1);
        		objectOutputStream.flush();
    		}
    		break;
    	}
    	
    	case "sterge secretar":
    	{
    		String id = (String) in.getObiecte().get(0);
    		int idi = Integer.parseInt(id);
    		PersistSecretar.delete(idi);
    		Comenzi rez=new Comenzi("sters",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "sterge dietetician":
    	{
    		String id = (String) in.getObiecte().get(0);
    		int idi = Integer.parseInt(id);
    		PersistDietetician.delete(idi);
    		Comenzi rez=new Comenzi("sters",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "stergeProgramare":
    	{
    		int id = (int) in.getObiecte().get(0);
    		PersistProgramare.delete(id);
    		Comenzi rez=new Comenzi("sters",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "update secretar":
    	{
    		int id = (int) in.getObiecte().get(0);
    		String nume= (String) in.getObiecte().get(1);
    		String mail= (String) in.getObiecte().get(2);
    		String tel= (String) in.getObiecte().get(3);
    		PersistSecretar.update(id, nume, mail, tel);
    		Comenzi rez=new Comenzi("updatat",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "update dietetician":
    	{
    		int id = (int) in.getObiecte().get(0);
    		String nume= (String) in.getObiecte().get(1);
    		String mail= (String) in.getObiecte().get(2);
    		String tel= (String) in.getObiecte().get(3);
    		String progr= (String) in.getObiecte().get(4);
    		PersistDietetician.update(id, nume, mail,progr ,tel);
    		Comenzi rez=new Comenzi("updatat",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "rapoarte":
    	{
    		ArrayList<Clienti> clienti = (ArrayList<Clienti>) PersistClienti.clienti();
    		ArrayList<Object> clienti1 = new ArrayList<Object>();
    		for(Clienti c:clienti)
    		{
    			clienti1.add(c);
    		}
    		Comenzi rez=new Comenzi("clienti",clienti1);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "sterge client":
    	{
    		int id = (int) in.getObiecte().get(0);
    		PersistClienti.delete(id);
    		Comenzi rez=new Comenzi("sters",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "edit client":
    	{
    		PersistClienti.update((int)in.getObiecte().get(0),(String) in.getObiecte().get(1), (int)in.getObiecte().get(2),(int) in.getObiecte().get(3),(String) in.getObiecte().get(4), (String) in.getObiecte().get(5));
    		Comenzi rez=new Comenzi("updatat",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "cautare nume dietetician":
    	{
    		String nume = (String) in.getObiecte().get(0);
    		int dietetician = (int) in.getObiecte().get(1);
    		ArrayList<Object> rez1 = PersistProgramare.serchClient(dietetician,nume);
    		ArrayList<Clienti> c=new ArrayList<Clienti>();
    		for(Object o :rez1)
    		{
    			c.add(PersistClienti.selectId((int) o));
    		}
    		
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < c.size(); i++)
    		{
    			System.out.println(c.get(i).getNume());
    			int id1=(c.get(i)).getId();
    			String nume1=(c.get(i)).getNume();
    			int varsta1=(c.get(i)).getVarsta();
    			int greutate1=(c.get(i)).getGreutate();
    			String dieta1=(c.get(i)).getDieta();
    			String telefon=(c.get(i)).getTelefon();
    			send.add(id1);
    			send.add(nume1);
    			send.add(varsta1);
    			send.add(greutate1);
    			if(dieta1==null)
    			{
    			dieta1=" ";
    			send.add(dieta1);
    			}
    			else
    			{
    				send.add(dieta1);
    			}
    			send.add(telefon);
    		}
    		Comenzi rez;
    		if(!send.isEmpty())
    		{
    		rez =new Comenzi("gasit",send);
    		}
    		else
    		{
    		rez =new Comenzi("negasit",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		
    		break;
    	}
    	
    	case "filtrare in functie de dieta":
    	{
    		String dieta = (String) in.getObiecte().get(1);
    		int id = (int) in.getObiecte().get(0);
    		
    		ArrayList<Object> iduri = new ArrayList<Object>();
    		iduri=PersistProgramare.filtrareDupaDieta(id, dieta);
    		ArrayList<Clienti> c=new ArrayList<Clienti>();
    		
    		for(Object o :iduri)
    		{
    			c.add(PersistClienti.selectId((int) o));
    		}
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < c.size(); i++)
    		{
    			System.out.println(c.get(i).getNume());
    			int id1=(c.get(i)).getId();
    			String nume1=(c.get(i)).getNume();
    			int varsta1=(c.get(i)).getVarsta();
    			int greutate1=(c.get(i)).getGreutate();
    			String dieta1=(c.get(i)).getDieta();
    			String telefon=(c.get(i)).getTelefon();
    			send.add(id1);
    			send.add(nume1);
    			send.add(varsta1);
    			send.add(greutate1);
    			if(dieta1==null)
    			{
    			dieta1=" ";
    			send.add(dieta1);
    			}
    			else
    			{
    				send.add(dieta1);
    			}
    			send.add(telefon);
    		}
    		Comenzi rez;
    		if(!send.isEmpty())
    		{
    		rez =new Comenzi("filtrat",send);
    		}
    		else
    		{
    		rez =new Comenzi("nefiltrat",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    		
    	}
    	
    	case "filtrare in functie de varsta":
    	{
    		int varsta = Integer.parseInt((String) in.getObiecte().get(1));
    		int id = (int) in.getObiecte().get(0);
    		
    		ArrayList<Object> iduri = new ArrayList<Object>();
    		iduri=PersistProgramare.filtrareDupaVarsta(id, varsta);
    		ArrayList<Clienti> c=new ArrayList<Clienti>();
    		
    		for(Object o :iduri)
    		{
    			c.add(PersistClienti.selectId((int) o));
    		}
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < c.size(); i++)
    		{
    			System.out.println(c.get(i).getNume());
    			int id1=(c.get(i)).getId();
    			String nume1=(c.get(i)).getNume();
    			int varsta1=(c.get(i)).getVarsta();
    			int greutate1=(c.get(i)).getGreutate();
    			String dieta1=(c.get(i)).getDieta();
    			String telefon=(c.get(i)).getTelefon();
    			send.add(id1);
    			send.add(nume1);
    			send.add(varsta1);
    			send.add(greutate1);
    			if(dieta1==null)
    			{
    			dieta1=" ";
    			send.add(dieta1);
    			}
    			else
    			{
    				send.add(dieta1);
    			}
    			send.add(telefon);
    		}
    		Comenzi rez;
    		if(!send.isEmpty())
    		{
    		rez =new Comenzi("filtrat",send);
    		}
    		else
    		{
    		rez =new Comenzi("nefiltrat",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "filtrare in functie de greutate":
    	{
    		int greutate = Integer.parseInt((String) in.getObiecte().get(1));
    		int id = (int) in.getObiecte().get(0);
    		
    		ArrayList<Object> iduri = new ArrayList<Object>();
    		iduri=PersistProgramare.filtrareDupaGreutate(id, greutate);
    		ArrayList<Clienti> c=new ArrayList<Clienti>();
    		
    		for(Object o :iduri)
    		{
    			c.add(PersistClienti.selectId((int) o));
    		}
    		
    		ArrayList<Object> send = new ArrayList<Object>();
    		
    		for (int i = 0; i < c.size(); i++)
    		{
    			System.out.println(c.get(i).getNume());
    			int id1=(c.get(i)).getId();
    			String nume1=(c.get(i)).getNume();
    			int varsta1=(c.get(i)).getVarsta();
    			int greutate1=(c.get(i)).getGreutate();
    			String dieta1=(c.get(i)).getDieta();
    			String telefon=(c.get(i)).getTelefon();
    			send.add(id1);
    			send.add(nume1);
    			send.add(varsta1);
    			send.add(greutate1);
    			if(dieta1==null)
    			{
    			dieta1=" ";
    			send.add(dieta1);
    			}
    			else
    			{
    				send.add(dieta1);
    			}
    			send.add(telefon);
    		}
    		Comenzi rez;
    		if(!send.isEmpty())
    		{
    		rez =new Comenzi("filtrat",send);
    		}
    		else
    		{
    		rez =new Comenzi("nefiltrat",send);
    		}
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "seteaza program":
    	{
    		String program = (String) in.getObiecte().get(0);
    		int id = (int) in.getObiecte().get(1);
    		Comenzi rez=new Comenzi("setat",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	case "setare dieta":
    	{
    		String dieta = (String) in.getObiecte().get(0);
    		int id = (int) in.getObiecte().get(1);
    		Clienti c =PersistClienti.selectId(id);
    		PersistClienti.update(id, c.getNume(), c.getVarsta(), c.getGreutate(), dieta, c.getTelefon());
    		Comenzi rez=new Comenzi("updatat",null);
    		out = socket.getOutputStream();
    		objectOutputStream = new ObjectOutputStream(out);
    		objectOutputStream.writeObject(rez);
    		objectOutputStream.flush();
    		break;
    	}
    	
    	default :
    	{
    		break;
    	}
    	
    	}

    	}
        
    } 
    
    public static void main(String args[]) throws IOException, ClassNotFoundException 
    { 
    	new Server();
    } 
} 