package model;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Comenzi implements Serializable{

	private volatile String comanda = "";
	private volatile ArrayList<Object> obiecte;
	
	public Comenzi(String c,ArrayList<Object> a)
	{
		setComanda(c);
		setObiecte(a);
	}

	public String getComanda() {
		return comanda;
	}
	
	public  void setComanda(String comanda) {
		this.comanda = comanda;
	}

	public ArrayList<Object> getObiecte() {
		return obiecte;
	}

	public  void setObiecte(ArrayList<Object> obiecte) {
		this.obiecte = obiecte;
	}
}
