package model;

import java.io.Serializable;


@SuppressWarnings("serial")
public class Dietetician extends User implements Serializable{
	
	private java.util.List<java.util.Map.Entry<Clienti,String>> programari= new java.util.ArrayList<>();
	private String program;
	
	public Dietetician(int id,String nume, String email, String parola, String program,String telefon) {
		super(id, nume, email, parola, telefon);
		// TODO Auto-generated constructor stub
		setProgram(program);
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

}


