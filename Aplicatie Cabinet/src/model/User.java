package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class User implements Serializable{
	
	private int id;
	private String nume;
	private String email;
	private String parola;
	private String telefon;
	
	public User(int id,String nume, String email, String parola, String telefon)
	{
		setId(id);
		setNume(nume);
		setEmail(email);
		setParola(parola);
		setTelefon(telefon);
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getParola() {
		return parola;
	}

	public void setParola(String parola) {
		this.parola = parola;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
