package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Clienti implements Serializable{
	
	private int id;
	private String nume;
	private int varsta;
	private int greutate;
	private String dieta;
	private String telefon;
	
	public Clienti(int id,String nume,int varsta,int greutate,String dieta,String telefon)
	{
		setId(id);
		setNume(nume);
		setVarsta(varsta);
		setGreutate(greutate);
		setDieta(dieta);
		setTelefon(telefon);
	}
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public int getVarsta() {
		return varsta;
	}
	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}
	public int getGreutate() {
		return greutate;
	}
	public void setGreutate(int greutate) {
		this.greutate = greutate;
	}
	public String getDieta() {
		return dieta;
	}
	public void setDieta(String dieta) {
		this.dieta = dieta;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() { 
	        return String.format(getNume() + ";" + getVarsta()+ ";"+getGreutate()+";"+getDieta()+";"+getTelefon()); 
	} 
	
}
