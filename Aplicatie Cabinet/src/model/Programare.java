package model;

public class Programare {

	private int id;
	private int id_dietet;
	private int id_client;
	private String data_ora;
	
	public Programare(int id,int dietet,int client,String data)
	{
		setId(id);
		setId_dietet(dietet);
		setId_client(client);
		setData_ora(data);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_dietet() {
		return id_dietet;
	}
	public void setId_dietet(int id_dietet) {
		this.id_dietet = id_dietet;
	}
	public int getId_client() {
		return id_client;
	}
	public void setId_client(int id_client) {
		this.id_client = id_client;
	}
	public String getData_ora() {
		return data_ora;
	}
	public void setData_ora(String data_ora) {
		this.data_ora = data_ora;
	}
	
}
