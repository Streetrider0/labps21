package view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import controller.InregistrareController;


public class InregistrareGui extends JFrame{
	
	JComboBox type;
	JTextField num,prenum,parola,maill,nt;
	JFrame f;
	
	public InregistrareGui()
	{
		createView();
		setTitle("Creare Cont Nou");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		f=this;
	}
	
	public void createView()
	{
		JPanel panel = new JPanel();
	    panel.setOpaque(false);
	    
		JPanel over = new JPanel();
		over.setPreferredSize(new Dimension(1080,720));
	    LayoutManager overlay = new OverlayLayout(over);
	    over.setLayout(overlay);
	    
	    JPanel imagen1 = new JPanel();
	    imagen1.setOpaque(false);
	    ImageIcon image = new ImageIcon("inregistrare.gif") ;
	    imagen1.add(new JLabel(image));
	    
	    JLabel titlu=new JLabel("Creeare cont",SwingConstants.CENTER);
	    titlu.setPreferredSize(new Dimension(1000,100));
	    titlu.setFont(new Font("Arial", Font.ITALIC, 70));
	    titlu.setForeground(new Color(204, 255, 255));
	    panel.add(titlu);
	    
	    JLabel t=new JLabel("Tipul de utilizator",SwingConstants.CENTER);
	    t.setPreferredSize(new Dimension(300,30));
	    t.setFont(new Font("Arial", Font.ITALIC, 30));
	    t.setForeground(new Color(204, 255, 255));
	    panel.add(t);
	    
	    String [] tipuri= {"dietetician","secretar","administrator"};
	    type = new JComboBox(tipuri);
	    type.setPreferredSize(new Dimension(200,50));
	    type.setFont(new Font("Arial", Font.ITALIC, 30));
	    type.setBackground(new Color(204, 255, 255));
	    panel.add(type);
	    
	    JLabel nume=new JLabel("Nume");
	    nume.setPreferredSize(new Dimension(600,50));
	    nume.setFont(new Font("Arial", Font.ITALIC, 30));
	    nume.setForeground(new Color(204, 255, 255));
	    panel.add(nume);
	    
	    num=new JTextField();
	    num.setOpaque(false);
	    num.setPreferredSize(new Dimension(600,40));
	    num.setFont(new Font("Arial", Font.BOLD, 30));
	    num.setForeground(new Color(204, 255, 255));
	    panel.add(num);
	    
	    JLabel prenume=new JLabel("Prenume");
	    prenume.setPreferredSize(new Dimension(600,50));
	    prenume.setFont(new Font("Arial", Font.ITALIC, 30));
	    prenume.setForeground(new Color(204, 255, 255));
	    panel.add(prenume);
	    
	    prenum=new JTextField();
	    prenum.setOpaque(false);
	    prenum.setPreferredSize(new Dimension(600,40));
	    prenum.setFont(new Font("Arial", Font.BOLD, 30));
	    prenum.setForeground(new Color(204, 255, 255));
	    panel.add(prenum);
	    
	    JLabel tel=new JLabel("Numar telefon");
	    tel.setPreferredSize(new Dimension(600,50));
	    tel.setFont(new Font("Arial", Font.ITALIC, 30));
	    tel.setForeground(new Color(204, 255, 255));
	    panel.add(tel);
	    
	    nt=new JTextField();
	    nt.setOpaque(false);
	    nt.setPreferredSize(new Dimension(600,40));
	    nt.setFont(new Font("Arial", Font.BOLD, 30));
	    nt.setForeground(new Color(204, 255, 255));
	    panel.add(nt);
	    
	    JLabel mail=new JLabel("Email");
	    mail.setPreferredSize(new Dimension(600,50));
	    mail.setFont(new Font("Arial", Font.ITALIC, 30));
	    mail.setForeground(new Color(204, 255, 255));
	    panel.add(mail);
	    
	    maill=new JTextField();
	    maill.setOpaque(false);
	    maill.setPreferredSize(new Dimension(600,40));
	    maill.setFont(new Font("Arial", Font.BOLD, 30));
	    maill.setForeground(new Color(204, 255, 255));
	    panel.add(maill);
	    
	    JLabel pas=new JLabel("Parola");
	    pas.setPreferredSize(new Dimension(600,50));
	    pas.setFont(new Font("Arial", Font.ITALIC, 30));
	    pas.setForeground(new Color(204, 255, 255));
	    panel.add(pas);
	    
	    parola=new JTextField();
	    parola.setOpaque(false);
	    parola.setPreferredSize(new Dimension(600,40));
	    parola.setFont(new Font("Arial", Font.BOLD, 30));
	    parola.setForeground(new Color(204, 255, 255));
	    panel.add(parola);

	    
	    JButton next=new JButton("Inregistrare");
	    next.setForeground(new Color(0, 153, 153));
	    next.setFont(new Font("Arial", Font.BOLD, 30));
	    next.setPreferredSize(new Dimension(250,50));
	    next.addActionListener(new Inregistrare());
	    panel.add(next);
	    
	    over.add(panel);
	    over.add(imagen1);
	    add(over);
	}
	
	public class Inregistrare implements ActionListener 
	{

		public void actionPerformed(ActionEvent e)
		{
			if(!num.getText().isEmpty() && !maill.getText().isEmpty() && ! parola.getText().isEmpty() && ! nt.getText().isEmpty())
			{
				String nume = num.getText()+" "+prenum.getText();
				String mail =  maill.getText();
				String pass = parola.getText();
				String telefon = nt.getText();
				String tip_user = (String) type.getSelectedItem();
			    InregistrareController.register(tip_user,nume,mail,pass,telefon,f);
			}
			else
			{
				new MesajGui("Introduceti toate campurile!");
			}
		}
		
	}
}
