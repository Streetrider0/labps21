package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.OverlayLayout;

import controller.SecretarController;

public class ClientiGUI extends JFrame{
	
	String operatie,nume,dieta,telefon;
	int id,varsta,greutate;
	JTextField numeTxt,varstaTxt,greutateTxt,telefonTxt;
	JLabel numeLab,varstaLab,greutateLab,telefonLab;
	
	public ClientiGUI(String operatie,int id,String nume,int varsta,int greutate,String dieta,String telefon)
	{
		this.operatie=operatie;
		this.id=id;
		this.nume=nume;
		this.dieta=dieta;
		this.telefon=telefon;
		this.varsta=varsta;
		this.greutate=greutate;
		createView();
		setTitle("Management Clienti");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public void createView()
	{
		
		JPanel label = new JPanel();
	    label.setOpaque(false);
	    
		JPanel over = new JPanel();
		over.setPreferredSize(new Dimension(600,300));
	    LayoutManager overlay = new OverlayLayout(over);
	    over.setLayout(overlay);
	    
	    JPanel imagen1 = new JPanel();
	    imagen1.setOpaque(false);
	    ImageIcon image = new ImageIcon("clienti.gif") ;
	    imagen1.add(new JLabel(image));
		
		numeLab= new JLabel("Nume Client");
		numeLab.setForeground(new Color(0, 230, 230));
		numeLab.setPreferredSize(new Dimension(250,50));
		numeLab.setFont(new Font("Arial", Font.BOLD, 21));
		label.add(numeLab);
		
		numeTxt = new JTextField();
		numeTxt.setForeground(new Color(0, 230, 230));
		numeTxt.setOpaque(false);
		numeTxt.setPreferredSize(new Dimension(250,50));
		numeTxt.setFont(new Font("Arial", Font.BOLD, 21));
		label.add(numeTxt);
		
		varstaLab= new JLabel("Varsta Client");
		varstaLab.setForeground(new Color(0, 230, 230));
		varstaLab.setPreferredSize(new Dimension(250,50));
		varstaLab.setFont(new Font("Arial", Font.BOLD, 21));
		label.add(varstaLab);
		
		varstaTxt = new JTextField();
		varstaTxt.setForeground(new Color(0, 230, 230));
		varstaTxt.setOpaque(false);
		varstaTxt.setPreferredSize(new Dimension(250,50));
		varstaTxt.setFont(new Font("Arial", Font.BOLD, 21));
		label.add(varstaTxt);
		
		greutateLab= new JLabel("Greutate Client");
		greutateLab.setForeground(new Color(0, 230, 230));
		greutateLab.setPreferredSize(new Dimension(250,50));
		greutateLab.setFont(new Font("Arial", Font.BOLD, 21));
		label.add(greutateLab);
		
		greutateTxt = new JTextField();
		greutateTxt.setForeground(new Color(0, 230, 230));
		greutateTxt.setOpaque(false);
		greutateTxt.setPreferredSize(new Dimension(250,50));
		greutateTxt.setFont(new Font("Arial", Font.BOLD, 21));
		label.add(greutateTxt);
		
		telefonLab= new JLabel("Telefon Client");
		telefonLab.setForeground(new Color(0, 230, 230));
		telefonLab.setPreferredSize(new Dimension(250,50));
		telefonLab.setFont(new Font("Arial", Font.BOLD, 21));
		label.add(telefonLab);
		
		telefonTxt = new JTextField();
		telefonTxt.setForeground(new Color(0, 230, 230));
		telefonTxt.setOpaque(false);
		telefonTxt.setPreferredSize(new Dimension(250,50));
		telefonTxt.setFont(new Font("Arial", Font.BOLD, 21));
		label.add(telefonTxt);
		
		switch(operatie)
		{
		case "Adauga":
			JButton add = new JButton("Adauga Client");
			add.setPreferredSize(new Dimension(600,50));
			add.setFont(new Font("Arial", Font.BOLD, 21));
			add.addActionListener(new AdaugaClient());
			label.add(add);
			break;
		default :
			numeTxt.setText(nume);
			varstaTxt.setText(String.valueOf(varsta));
			greutateTxt.setText(String.valueOf(greutate));
			telefonTxt.setText(telefon);
			JButton update= new JButton("Modifica Client");
			update.setPreferredSize(new Dimension(600,50));
			update.setFont(new Font("Arial", Font.BOLD, 21));
			update.addActionListener(new EditClient());
			label.add(update);
			break;
		}
		
		over.add(label);
	    over.add(imagen1);
	    add(over);
	}
	
	public class AdaugaClient implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(!numeTxt.getText().isEmpty() && !varstaTxt.getText().isEmpty() && !greutateTxt.getText().isEmpty() && !telefonTxt.getText().isEmpty())
			{
			SecretarController.insertClient(numeTxt.getText(),Integer.parseInt(varstaTxt.getText()),Integer.parseInt(greutateTxt.getText()),telefonTxt.getText());
			}
			else
			{
				new MesajGui("Introduceti toate campurile!");
			}
		}

	}
	
	public class EditClient implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(!numeTxt.getText().isEmpty() && !varstaTxt.getText().isEmpty() && !greutateTxt.getText().isEmpty() && !telefonTxt.getText().isEmpty())
			{
			SecretarController.editClient(id,numeTxt.getText(),Integer.parseInt(varstaTxt.getText()),Integer.parseInt(greutateTxt.getText()),dieta,telefonTxt.getText());
			}
			else
			{
				new MesajGui("Introduceti toate campurile!");
			}
		}

	}

}
