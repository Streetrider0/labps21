package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.*;

import client.*;
import controller.LoginController;
import model.User;


public class LoginGui extends JFrame{

	JPanel panel;
	JLabel buna, mail,parola;
	JButton logare,creeare;
	JPasswordField pas;
	JTextField email;
	
	public LoginGui()
	{
		createView();
		setTitle("Logare");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void createView()
	{
        
		JPanel panel = new JPanel();
	    panel.setOpaque(false);
	    
		JPanel over = new JPanel();
		over.setPreferredSize(new Dimension(1080,720));
	    LayoutManager overlay = new OverlayLayout(over);
	    over.setLayout(overlay);
	    
	    JPanel imagen1 = new JPanel();
	    imagen1.setOpaque(false);
	    ImageIcon image = new ImageIcon("logare.gif") ;
	    imagen1.add(new JLabel(image));
	    
	    Date dateAndTime = Calendar.getInstance().getTime();
	    SimpleDateFormat formatter= new SimpleDateFormat("dd.MM.yyyy");
	    JLabel dateTime = new JLabel(formatter.format(dateAndTime).toString(),SwingConstants.RIGHT);
	    dateTime.setPreferredSize(new Dimension(800,30));
		dateTime.setFont(new Font("Arial", Font.ITALIC, 30));
		dateTime.setForeground(new Color(204, 255, 255));
	    panel.add(dateTime);

	    buna = new JLabel("Logare",SwingConstants.CENTER);
	    buna.setForeground(new Color(204, 255, 255));
		buna.setFont(new Font("Arial", Font.ITALIC, 100));
		buna.setAlignmentX(0);
		buna.setPreferredSize(new Dimension(800,300));
		panel.add(buna);
		
		mail = new JLabel("E-mail");
		mail.setForeground(new Color(204, 255, 255));
		mail.setFont(new Font("Arial", Font.BOLD, 30));
		mail.setAlignmentX(0);
		mail.setPreferredSize(new Dimension(800,50));
		panel.add(mail);
		
	    email = new JTextField();
	    email.setForeground(new Color(0, 0, 153));
	    email.setOpaque(false);
		email.setFont(new Font("Arial", Font.BOLD, 30));
		email.setPreferredSize(new Dimension(800,50));
		panel.add(email);
		
		parola = new JLabel("Parola");
		parola.setForeground(new Color(204, 255, 255));
		parola.setFont(new Font("Arial", Font.BOLD, 30));
		parola.setAlignmentX(0);
		parola.setPreferredSize(new Dimension(800,50));
		panel.add(parola);
		
		pas = new JPasswordField();
		pas.setEchoChar('�');
		pas.setForeground(new Color(0, 0, 153));
		pas.setOpaque(false);
		pas.setFont(new Font("Arial", Font.BOLD, 60));
		pas.setPreferredSize(new Dimension(800,50));
		panel.add(pas);
		
		logare = new JButton("Autentificare");
		logare.setForeground(new Color(0, 184, 230));
		logare.setFont(new Font("Arial", Font.BOLD, 30));
		logare.setPreferredSize(new Dimension(400,50));
		logare.addActionListener(new Autentificare());
		panel.add(logare);
		
		creeare = new JButton("Inregistrare");
		creeare .setForeground(new Color(0, 184, 230));
		creeare .setFont(new Font("Arial", Font.BOLD, 30));
		creeare .setPreferredSize(new Dimension(400,50));
		creeare.addActionListener(new Creeare());
		panel.add(creeare );
		
	    over.add(panel);
	    over.add(imagen1);
	    add(over);
	}
	
	

	private class Autentificare implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			LoginController.loginRequest(email.getText(),pas.getText());
		}
		
	}
	
	private class Creeare implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			new InregistrareGui();
		}
		
	}
}