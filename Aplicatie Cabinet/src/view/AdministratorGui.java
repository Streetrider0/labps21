package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.OverlayLayout;
import javax.swing.table.DefaultTableModel;

import controller.AdministratorController;

public class AdministratorGui extends JFrame{
	JTextField num,prenum,parola,maill,nt;
	JFrame f;
	static JTable tabel1;
	static JTable tabel2;
	JScrollPane scroll1,scroll2;
	String nume,mail;
	static JTextField numeUser,email,telefon,program;
	JButton update;
	String tip;
	int id;
	
	public AdministratorGui(String nume,String mail)
	{
		this.nume=nume;
		this.mail=mail;
		createView();
		setTitle("Panou Administrator");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		f=this;
	}
	
	public static void updateTabele()
	{
		String col1[] = {"Id","Nume","Email","Telefon"};
		DefaultTableModel t = new DefaultTableModel(col1,0);
	    AdministratorController.tabel("tabel secretari", t);
	    tabel1.setModel(t);
	    
	    String col2[] = {"Id","Nume","Email","Telefon","Program"};
		DefaultTableModel t1 = new DefaultTableModel(col2,0);
	    AdministratorController.tabel("tabel dieteticieni", t1);
	    tabel2.setModel(t1);
	    
	    numeUser.setText(null);
	    email.setText(null);
	    telefon.setText(null);
	    telefon.setText(null);
	    program.setText(null);
	}
	
	public void createView()
	{
		JPanel panel = new JPanel();
	    panel.setOpaque(false);
	    
		JPanel over = new JPanel();
		over.setPreferredSize(new Dimension(1080,720));
	    LayoutManager overlay = new OverlayLayout(over);
	    over.setLayout(overlay);
	    
	    JPanel imagen1 = new JPanel();
	    imagen1.setOpaque(false);
	    ImageIcon image = new ImageIcon("admin.gif") ;
	    imagen1.add(new JLabel(image));
	    
	    ImageIcon image1 = new ImageIcon("admin.png");
	    JLabel icon = new JLabel(image1);
	    icon.setPreferredSize(new Dimension(1000,120));
	    panel.add(icon);
	    
	    JLabel numep = new JLabel(nume);
	    numep.setPreferredSize(new Dimension(1000,20));
	    numep.setFont(new Font("Arial", Font.BOLD, 21));
	    numep.setForeground(new Color(0, 230, 230));
	    panel.add(numep);
	    
	    JLabel mailp = new JLabel(mail);
	    mailp.setPreferredSize(new Dimension(1000,25));
	    mailp.setFont(new Font("Arial", Font.BOLD, 21));
	    mailp.setForeground(new Color(0, 230, 230));
	    panel.add(mailp);
	    
	    JLabel adminp = new JLabel("Administrator");
	    adminp .setPreferredSize(new Dimension(1000,20));
	    adminp.setFont(new Font("Arial", Font.BOLD, 21));
	    adminp.setForeground(new Color(0, 230, 230));
	    panel.add(adminp );
	    
		String col[] = {"Id","Nume","Email","Telefon"};
	    DefaultTableModel tableModel1 =new DefaultTableModel(col, 0);
	    AdministratorController.tabel("tabel secretari",tableModel1);
	    this.tabel1 = new JTable(tableModel1);
	    scroll1 = new JScrollPane(this.tabel1);
		scroll1.setPreferredSize(new Dimension(500,200));
		scroll1.setBorder(BorderFactory.createTitledBorder ("Secretari"));
		panel.add(scroll1);
		
		String col1[] = {"Id","Nume","Email","Telefon","Program"};
	    DefaultTableModel tableModel2 = new DefaultTableModel(col1, 0);
	    AdministratorController.tabel("tabel dieteticieni",tableModel2);
	    this.tabel2 = new JTable(tableModel2);
	    scroll2 = new JScrollPane(this.tabel2);
		scroll2.setPreferredSize(new Dimension(500,200));
		scroll2.setBorder(BorderFactory.createTitledBorder ("Dieteticieni"));
		panel.add(scroll2);
	    
		JButton deleteSecretar = new JButton("Sterge Cont Secretar");
		deleteSecretar.setFont(new Font("Arial", Font.BOLD, 21));
		deleteSecretar.setPreferredSize(new Dimension(500,50));
		deleteSecretar.addActionListener(new StergeSecretar());
		panel.add(deleteSecretar);
		
		JButton deleteDietetician = new JButton("Sterge Cont Dietetician");
		deleteDietetician.setFont(new Font("Arial", Font.BOLD, 21));
		deleteDietetician.setPreferredSize(new Dimension(500,50));
		deleteDietetician.addActionListener(new StergeDietetician());
		panel.add(deleteDietetician);
		
		JButton editareSecretar = new JButton("Editeaza Cont Secretar");
		editareSecretar.setFont(new Font("Arial", Font.BOLD, 21));
		editareSecretar.setPreferredSize(new Dimension(500,50));
		editareSecretar.addActionListener(new EditSecretar());
		panel.add(editareSecretar);
		
		JButton editareDietetician = new JButton("Editeaza Cont Dietetician");
		editareDietetician.setFont(new Font("Arial", Font.BOLD, 21));
		editareDietetician.setPreferredSize(new Dimension(500,50));
		editareDietetician.addActionListener(new EditDietetician());
		panel.add(editareDietetician);
		
		numeUser = new JTextField();
		numeUser.setFont(new Font("Arial", Font.BOLD, 21));
		numeUser.setOpaque(false);
		numeUser.setForeground(new Color(0, 230, 230));
		numeUser.setPreferredSize(new Dimension(500,30));
		panel.add(numeUser);
		
		email = new JTextField();
		email.setFont(new Font("Arial", Font.BOLD, 21));
		email.setOpaque(false);
		email.setForeground(new Color(0, 230, 230));
		email.setPreferredSize(new Dimension(500,30));
		panel.add(email);
		
		telefon = new JTextField();
		telefon.setFont(new Font("Arial", Font.BOLD, 21));
		telefon.setOpaque(false);
		telefon.setForeground(new Color(0, 230, 230));
		telefon.setPreferredSize(new Dimension(500,30));
		panel.add(telefon);
		
		program = new JTextField();
		program.setFont(new Font("Arial", Font.BOLD, 21));
		program.setOpaque(false);
		program.setForeground(new Color(0, 230, 230));
		program.setPreferredSize(new Dimension(500,30));
		panel.add(program);
		
		update = new JButton("Modifica");
		update.setPreferredSize(new Dimension(1000,50));
		update.setFont(new Font("Arial", Font.BOLD, 21));
		update.setVisible(false);
		update.addActionListener(new Update());
		panel.add(update);
		
	    over.add(panel);
	    over.add(imagen1);
	    add(over);
	}
	

	public class StergeSecretar implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
			int row = tabel1.getSelectedRow();
			String res =tabel1.getModel().getValueAt(row, 0).toString();
		    AdministratorController.sterge("sterge secretar",res);
			}
			catch(Exception NullPointerException)
			{
				new MesajGui("Selectati secretarul pe care doriti sa il stergeti!");
			}
		}

	}
	
	public class StergeDietetician implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
			int row = tabel2.getSelectedRow();
			String res =tabel2.getModel().getValueAt(row, 0).toString();
		    AdministratorController.sterge("sterge dietetician",res);
			}
			catch(Exception NullPointerException)
			{
				new MesajGui("Selectati dieteticianul pe care doriti sa il stergeti!");
			}
		}

	}
	
	public class EditSecretar implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try
			{
			int row = tabel1.getSelectedRow();
			id=Integer.parseInt(tabel1.getModel().getValueAt(row, 0).toString());
			numeUser.setText(tabel1.getModel().getValueAt(row, 1).toString());
			email.setText(tabel1.getModel().getValueAt(row, 2).toString());
			telefon.setText(tabel1.getModel().getValueAt(row, 3).toString());
			tip="Secretar";
			update.setVisible(true);
			}
			catch(Exception NullPointerException)
			{
				new MesajGui("Selectati secretarul pentru editare!");
			}
		}

	}
	
	public class EditDietetician implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try
			{
			int row = tabel2.getSelectedRow();
			id=Integer.parseInt(tabel2.getModel().getValueAt(row, 0).toString());
			numeUser.setText(tabel2.getModel().getValueAt(row, 1).toString());
			email.setText(tabel2.getModel().getValueAt(row, 2).toString());
			telefon.setText(tabel2.getModel().getValueAt(row, 3).toString());
			try {
			program.setText(tabel2.getModel().getValueAt(row, 4).toString());
			}
			catch(Exception NullPointerException)
			{
				program.setText("Program nedefinit");
			}
			tip="Dietetician";
			update.setVisible(true);
			}
			catch(Exception NullPointerException)
			{
				new MesajGui("Selectati dieteticianul pentru editare!");
			}
		}

	}
	
	public class Update implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			switch(tip)
			{
			case "Secretar":
				String nume=numeUser.getText();
				String mail=email.getText();
				String tel=telefon.getText();
				if(!nume.contentEquals("") && !mail.contentEquals("") && !tel.contentEquals(""))
				{
				AdministratorController.updateSecretar(id,nume,mail,tel);
				update.setVisible(false);
				}
				else
				{
					new MesajGui("Introduceti toate fieldurile!");
				}
				break;
			case "Dietetician":
				String nume1=numeUser.getText();
				String mail1=email.getText();
				String tel1=telefon.getText();
				String progr=program.getText();
				if(!nume1.contentEquals("") && !mail1.contentEquals("") && !tel1.contentEquals("") && !progr.contentEquals(""))
				{
				AdministratorController.updateDietetician(id,nume1,mail1,tel1,progr);
				update.setVisible(false);
				}
				else
				{
					new MesajGui("Introduceti toate fieldurile!");
				}
				break;
			default:
				break;
			}
		}
		
	}

}
