package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.OverlayLayout;
import javax.swing.table.DefaultTableModel;

import controller.SecretarController;

public class SecretarGui extends JFrame{

	String email,nume;
	static JTable tabel1,tabel2,tabel3;
	JScrollPane scroll1,scroll2,scroll3;
	JComboBox ora,filtrare;
	JTextField data,numet,filtru;
	
	public SecretarGui(String nume,String email)
	{
		this.nume=nume;
		this.email=email;
		createView();
		setTitle("Panou Secretar");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public static void updateTabele()
	{
		String col1[] = {"Id","Nume","Varsta","Greutate","Dieta","Telefon"};
		DefaultTableModel t = new DefaultTableModel(col1,0);
		SecretarController.tabel("tabel clienti", t);
	    tabel1.setModel(t);
	    
	    String col2[] = {"Id","Nume","Email","Telefon","Program"};
		DefaultTableModel t1 = new DefaultTableModel(col2,0);
		SecretarController.tabel("tabel dieteticieni", t1);
	    tabel2.setModel(t1);
	    
	    String col3[] = {"Id","Id Dietetician","Id Client","Data si Ora"};
	    DefaultTableModel t2 = new DefaultTableModel(col3,0);
	    SecretarController.tabel("tabel programari", t2);
	    tabel3.setModel(t2);
	}
	
	public void createView()
	{
		JPanel panel = new JPanel();
	    panel.setOpaque(false);
	    
		JPanel over = new JPanel();
		over.setPreferredSize(new Dimension(1080,800));
	    LayoutManager overlay = new OverlayLayout(over);
	    over.setLayout(overlay);
	    
	    JPanel imagen1 = new JPanel();
	    imagen1.setOpaque(false);
	    ImageIcon image = new ImageIcon("secretar.gif") ;
	    imagen1.add(new JLabel(image));
	    
	    ImageIcon image1 = new ImageIcon("secretar.png");
	    JLabel icon = new JLabel(image1);
	    icon.setPreferredSize(new Dimension(1000,100));
	    panel.add(icon);
	    
	    JLabel numep = new JLabel(nume);
	    numep.setPreferredSize(new Dimension(1000,20));
	    numep.setFont(new Font("Arial", Font.BOLD, 21));
	    numep.setForeground(new Color(0, 230, 230));
	    panel.add(numep);
	    
	    JLabel mailp = new JLabel(email);
	    mailp.setPreferredSize(new Dimension(1000,20));
	    mailp.setFont(new Font("Arial", Font.BOLD, 21));
	    mailp.setForeground(new Color(0, 230, 230));
	    panel.add(mailp);
	    
	    JLabel adminp = new JLabel("Secretar");
	    adminp .setPreferredSize(new Dimension(1000,20));
	    adminp.setFont(new Font("Arial", Font.BOLD, 21));
	    adminp.setForeground(new Color(0, 230, 230));
	    panel.add(adminp );
	    
		String col[] = {"Id","Nume","Varsta","Greutate","Dieta","Telefon"};
	    DefaultTableModel tableModel1 =new DefaultTableModel(col, 0);
	    SecretarController.tabel("tabel clienti",tableModel1);
	    this.tabel1 = new JTable(tableModel1);
	    scroll1 = new JScrollPane(this.tabel1);
		scroll1.setPreferredSize(new Dimension(500,200));
		scroll1.setBorder(BorderFactory.createTitledBorder ("Clienti"));
		panel.add(scroll1);
		
		String col1[] = {"Id","Nume","Email","Telefon","Program"};
	    DefaultTableModel tableModel2 =new DefaultTableModel(col1, 0);
	    SecretarController.tabel("tabel dieteticieni",tableModel2);
	    this.tabel2 = new JTable(tableModel2);
	    scroll2 = new JScrollPane(this.tabel2);
		scroll2.setPreferredSize(new Dimension(500,200));
		scroll2.setBorder(BorderFactory.createTitledBorder ("Dieteticieni"));
		panel.add(scroll2);
		
		JPanel centru = new JPanel();
		centru.setPreferredSize(new Dimension(1000,200));
		
		String col2[] = {"Id","Id Dietetician","Id Client","Data si Ora"};
	    DefaultTableModel tableModel3 =new DefaultTableModel(col2, 0);
	    SecretarController.tabel("tabel programari",tableModel3);
	    this.tabel3 = new JTable(tableModel3);
	    scroll3 = new JScrollPane(this.tabel3);
		scroll3.setPreferredSize(new Dimension(500,200));
		scroll3.setBorder(BorderFactory.createTitledBorder ("Programari"));
		centru.setOpaque(false);
		centru.add(scroll3);
		panel.add(centru);
		
		JButton addClient = new JButton("Adauga Client");
		addClient.setPreferredSize(new Dimension(330,30));
		addClient.setFont(new Font("Arial", Font.BOLD, 21));
		addClient.addActionListener(new AdaugaClient());
		panel.add(addClient);
		
		JButton delClient = new JButton("Sterge Client");
		delClient.setPreferredSize(new Dimension(330,30));
		delClient.setFont(new Font("Arial", Font.BOLD, 21));
		delClient.addActionListener(new StergeClient());
		panel.add(delClient);
		
		JButton editClient = new JButton("Modifica date client");
		editClient.setPreferredSize(new Dimension(330,30));
		editClient.setFont(new Font("Arial", Font.BOLD, 21));
		editClient.addActionListener(new ModificaClient());
		panel.add(editClient);
		
		JButton totiClientii = new JButton("Afiseaza toti clientii");
		totiClientii.setPreferredSize(new Dimension(330,30));
		totiClientii.setFont(new Font("Arial", Font.BOLD, 21));
		totiClientii.addActionListener(new TotiClientii());
		panel.add(totiClientii);
		
		numet = new JTextField("Nume Client");
		numet.setPreferredSize(new Dimension(330,30));
		numet.setFont(new Font("Arial", Font.BOLD, 21));
		numet.setForeground(new Color(0, 230, 230));
		numet.setOpaque(false);
		 panel.add(numet);
		 
		 JButton cautaNume = new JButton("Cautare Client");
		 cautaNume.setPreferredSize(new Dimension(330,30));
		 cautaNume.setFont(new Font("Arial", Font.BOLD, 21));
		 cautaNume.addActionListener(new CautaNume());
		 panel.add(cautaNume);
		 
		 String [] tipuri1= {"Dietetician","Dieta","Varsta","Greutate"};
		 filtrare = new JComboBox(tipuri1);
		 filtrare.setOpaque(false);
		 filtrare.setPreferredSize(new Dimension(330,30));
		 filtrare.setFont(new Font("Arial", Font.ITALIC, 30));
		 filtrare.setBackground(new Color(0, 230, 230));
		 panel.add(filtrare);
		 
		 filtru=new JTextField("Parametru filtrare");
		 filtru.setPreferredSize(new Dimension(330,30));
		 filtru.setFont(new Font("Arial", Font.BOLD, 21));
		 filtru.setForeground(new Color(0, 230, 230));
		 filtru.setOpaque(false);
		 panel.add(filtru);
		 
		 JButton filreaza = new JButton("Filtreaza Clienti");
		 filreaza.setPreferredSize(new Dimension(330,30));
		 filreaza.setFont(new Font("Arial", Font.BOLD, 21));
		 filreaza.addActionListener(new Filtreaza());
		 panel.add(filreaza);
		
		 String [] tipuri= {"08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00","17:00","18:00"};
		 ora = new JComboBox(tipuri);
		 ora.setOpaque(false);
		 ora.setPreferredSize(new Dimension(150,30));
		 ora.setFont(new Font("Arial", Font.ITALIC, 30));
		 ora.setBackground(new Color(0, 230, 230));
		 panel.add(ora);
		 
		 data=new JTextField("Data");
		 data.setPreferredSize(new Dimension(150,30));
		 data.setFont(new Font("Arial", Font.BOLD, 21));
		 data.setForeground(new Color(0, 230, 230));
		 data.setOpaque(false);
		 panel.add(data);
		
		JButton programeaza = new JButton("Programare noua");
		programeaza.setPreferredSize(new Dimension(330,30));
		programeaza.setFont(new Font("Arial", Font.BOLD, 21));
		programeaza.addActionListener(new Programeaza());
		panel.add(programeaza);
		
		JButton stergeProgr = new JButton("Anuleaza programarea");
		stergeProgr.setPreferredSize(new Dimension(330,30));
		stergeProgr.setFont(new Font("Arial", Font.BOLD, 21));
		stergeProgr.addActionListener(new StergeProgramare());
		panel.add(stergeProgr);
		
		JButton rapoarte = new JButton("Salvati Rapoarte Clienti");
		rapoarte.setPreferredSize(new Dimension(1000,30));
		rapoarte.setFont(new Font("Arial", Font.BOLD, 21));
		rapoarte.addActionListener(new SalvareRapoarte());
		panel.add(rapoarte);
		
		
	    over.add(panel);
	    over.add(imagen1);
	    add(over);
	}
	
	public class AdaugaClient implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			new ClientiGUI("Adauga",0,null,0,0,null,null);
		}

	}
	
	public class ModificaClient implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
				int row = tabel1.getSelectedRow();
				int id=Integer.parseInt(tabel1.getModel().getValueAt(row, 0).toString());
				String nume=tabel1.getModel().getValueAt(row, 1).toString();
				int varsta=Integer.parseInt(tabel1.getModel().getValueAt(row, 2).toString());
				int greutate=Integer.parseInt(tabel1.getModel().getValueAt(row, 3).toString());
				String dieta=tabel1.getModel().getValueAt(row, 4).toString();
				String telefon=tabel1.getModel().getValueAt(row, 5).toString();
				new ClientiGUI("Modifica",id,nume,varsta,greutate,dieta,telefon);
				}
				catch( Exception ArrayIndexOutOfBoundsException)
				{
					new MesajGui("Selectati un client!");
				}
		}

	}
	
	public class StergeClient implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			try {
			int row = tabel1.getSelectedRow();
			int id=Integer.parseInt(tabel1.getModel().getValueAt(row, 0).toString());
			System.out.println(id);
			SecretarController.stergeClient(id);
			}
			catch( Exception ArrayIndexOutOfBoundsException)
			{
				new MesajGui("Selectati un client!");
			}
		}

	}
	
	public class Programeaza implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (data.getText().matches("([0-9]{2})/([0-9]{2})/([0-9]{4})"))
			{
				String time = (String) ora.getSelectedItem();
				try {
				int row = tabel1.getSelectedRow();
				int id_client=Integer.parseInt(tabel1.getModel().getValueAt(row, 0).toString());
				row = tabel2.getSelectedRow();
				int id_dietetician=Integer.parseInt(tabel2.getModel().getValueAt(row, 0).toString());
				SecretarController.programeaza(id_dietetician,id_client,time+" ; "+data.getText());
				}
				catch (Exception NullPointerException)
				{
					new MesajGui("Selectati Clientul si Dieteticianul dorit!");
				}
			}
			else
			{
				new MesajGui("Introduceti o data valida!");
			}
		}
		}
		
		public class StergeProgramare implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				try
				{
				int row = tabel3.getSelectedRow();
				int id=Integer.parseInt(tabel3.getModel().getValueAt(row, 0).toString());
				SecretarController.stergeProgramare(id);
				}
				catch(Exception NullPointerException)
				{
					new MesajGui("Selectati o programare!");
				}
				
			}

		}
		
		public class SalvareRapoarte implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					SecretarController.rapoarte();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		}
		
		public class CautaNume implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try
				{
				String col1[] = {"Id","Nume","Varsta","Greutate","Dieta","Telefon"};
				DefaultTableModel t = new DefaultTableModel(col1,0);
				System.out.println(numet.getText());
				SecretarController.cautaNume(numet.getText(),t);
				tabel1.setModel(t);
				}
				catch(Exception e1)
				{
					new MesajGui("Introduceti un nume de client!");
				}
			}

		}
		
		public class TotiClientii implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateTabele();
			}

		}
		
		public class Filtreaza implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String param = null;
				if((filtru.getText().contentEquals("Parametru filtrare") || filtru.getText().isEmpty() )&&filtrare.getSelectedItem().equals("Dietetician"))
				{
					try
					{
					int row = tabel2.getSelectedRow();
					String id=tabel2.getModel().getValueAt(row, 0).toString();
					param=id;
					}
					catch(Exception NullPointerException)
					{
						new MesajGui("Intoruceti un id valid sau selectati un dietetician din tabel!");
					}
				
				}
				if((!filtru.getText().contentEquals("Parametru filtrare") && !filtru.getText().isEmpty()) || param!=null)
				{
					if(param==null)
					{
						param=filtru.getText();
					}
				String col1[] = {"Id","Nume","Varsta","Greutate","Dieta","Telefon"};
				DefaultTableModel t = new DefaultTableModel(col1,0);
				SecretarController.filtrare((String)filtrare.getSelectedItem(), param,t);
				tabel1.setModel(t);
				}
				else
				{
					new MesajGui("Introduceti field-ul pentru filtrare!");
				}
			}

		}

}
