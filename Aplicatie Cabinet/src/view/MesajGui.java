package view;

import java.awt.*;
import javax.swing.*;

import controller.LoginController;

public class MesajGui extends JFrame{
	
	String msg;
	JPanel panel;
	JLabel text;
	
	public MesajGui(String msg)
	{
		this.msg=msg;
		createView();
		setTitle(msg);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void createView()
	{
        
		panel = new JPanel();
		panel.setBackground(new Color(153, 204, 255));
		panel.setPreferredSize(new Dimension(800,200));
		getContentPane().add(panel);
		
		text = new JLabel(msg,SwingConstants.CENTER);
		text.setPreferredSize(new Dimension(800,200));
		text.setForeground(Color.white);
		text.setFont(new Font("Arial", Font.BOLD, 22));
		panel.add(text);
	
	}
}
