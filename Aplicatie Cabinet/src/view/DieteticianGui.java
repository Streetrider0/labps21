package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.OverlayLayout;
import javax.swing.table.DefaultTableModel;

import controller.DieteticianController;
import controller.SecretarController;
import view.SecretarGui.AdaugaClient;
import view.SecretarGui.ModificaClient;
import view.SecretarGui.Programeaza;
import view.SecretarGui.SalvareRapoarte;
import view.SecretarGui.StergeClient;
import view.SecretarGui.StergeProgramare;

public class DieteticianGui extends JFrame{

	static int id;
	String nume,email;
	static JTable tabel1;
	static JTable tabel2;
	JScrollPane scroll1,scroll2;
	JComboBox dieta,criteriu;
	JTextField filtru,name,program;
	
	public DieteticianGui(String nume,String email,int id)
	{
		this.id=id;
		this.nume=nume;
		this.email=email;
		createView();
		setTitle("Panou Dietetician");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	
	
	public static void updateTabele()
	{
		String col1[] = {"Id","Nume","Varsta","Greutate","Dieta","Telefon"};
		DefaultTableModel t = new DefaultTableModel(col1,0);
		DieteticianController.tabel("Dietetician",t,id);
	    tabel1.setModel(t);
	    
	    String col3[] = {"Id","Id Dietetician","Id Client","Data si Ora"};
	    DefaultTableModel t2 = new DefaultTableModel(col3,0);
	    DieteticianController.tabel("DieteticianProgramari",t2,id);
	    tabel2.setModel(t2);
	}
	
	public void createView()
	{
		JPanel panel = new JPanel();
	    panel.setOpaque(false);
	    
		JPanel over = new JPanel();
		over.setPreferredSize(new Dimension(1080,720));
	    LayoutManager overlay = new OverlayLayout(over);
	    over.setLayout(overlay);
	    
	    JPanel imagen1 = new JPanel();
	    imagen1.setOpaque(false);
	    ImageIcon image = new ImageIcon("dietetician.gif") ;
	    imagen1.add(new JLabel(image));
	    
	    ImageIcon image1 = new ImageIcon("dietetician.png");
	    JLabel icon = new JLabel(image1);
	    icon.setPreferredSize(new Dimension(1000,100));
	    panel.add(icon);
	    
	    JLabel numep = new JLabel(nume);
	    numep.setPreferredSize(new Dimension(1000,20));
	    numep.setFont(new Font("Arial", Font.BOLD, 21));
	    numep.setForeground(new Color(51, 51, 255));
	    panel.add(numep);
	    
	    JLabel mailp = new JLabel(email);
	    mailp.setPreferredSize(new Dimension(1000,25));
	    mailp.setFont(new Font("Arial", Font.BOLD, 21));
	    mailp.setForeground(new Color(51, 51, 255));
	    panel.add(mailp);
	    
	    JLabel adminp = new JLabel("Dietetician");
	    adminp .setPreferredSize(new Dimension(1000,20));
	    adminp.setFont(new Font("Arial", Font.BOLD, 21));
	    adminp.setForeground(new Color(51, 51, 255));
	    panel.add(adminp );
	    
	    String col[] = {"Id","Nume","Varsta","Greutate","Dieta","Telefon"};
	    DefaultTableModel tableModel1 =new DefaultTableModel(col, 0);
	    DieteticianController.tabel("Dietetician",tableModel1,id);
	    this.tabel1 = new JTable(tableModel1);
	    scroll1 = new JScrollPane(this.tabel1);
		scroll1.setPreferredSize(new Dimension(500,200));
		scroll1.setBorder(BorderFactory.createTitledBorder ("Clienti"));
		panel.add(scroll1);
		
		String col2[] = {"Id","Id Dietetician","Id Client","Data si Ora"};
	    DefaultTableModel tableModel3 =new DefaultTableModel(col2, 0);
	    DieteticianController.tabel("DieteticianProgramari",tableModel3,id);
	    this.tabel2 = new JTable(tableModel3);
	    scroll2 = new JScrollPane(this.tabel2);
		scroll2.setPreferredSize(new Dimension(500,200));
		scroll2.setBorder(BorderFactory.createTitledBorder ("Programari"));
		panel.add(scroll2);
		
		name=new JTextField("Nume Client");
		name.setFont(new Font("Arial", Font.BOLD, 30));
		name.setPreferredSize(new Dimension(500,30));
		name.setOpaque(false);
		name.setForeground(new Color(51, 51, 255));
		panel.add(name);
		
		JButton cautaNume=new JButton("Cautare Client");
		cautaNume.setFont(new Font("Arial", Font.BOLD, 30));
		cautaNume.setPreferredSize(new Dimension(500,30));
		cautaNume.setForeground(new Color(51, 51, 255));
		cautaNume.addActionListener(new cautaNume());
		panel.add( cautaNume);
		
		JButton vizualizare =new JButton("Vizualizare clienti si program de consultatii");
		vizualizare.setPreferredSize(new Dimension(1000,40));
		vizualizare.setFont(new Font("Arial", Font.ITALIC, 30));
		vizualizare.setForeground(new Color(0, 0, 230));
		vizualizare.addActionListener(new Vizualizare());
		panel.add(vizualizare);
		
		String [] diete= {"Ketogenica","Vegetariana","Vegana","Montignac","Paleo","Rina"};
		dieta = new JComboBox(diete);
		dieta.setOpaque(false);
		dieta.setPreferredSize(new Dimension(500,40));
		dieta.setFont(new Font("Arial", Font.ITALIC, 30));
		dieta.setForeground(Color.white);
		dieta.setBackground(new Color(51, 51, 255));
		panel.add(dieta);
		
		JButton diet = new JButton("Stabileste dieta client");
		diet.setPreferredSize(new Dimension(500,40));
		diet.setFont(new Font("Arial", Font.ITALIC, 30));
		diet.setForeground(new Color(0, 0, 230));
		diet.addActionListener(new SetDieta());
		panel.add(diet);
		
		String [] tipuri= {"Varsta","Greutate","Dieta"};
		criteriu = new JComboBox(tipuri);
		criteriu.setOpaque(false);
		criteriu.setPreferredSize(new Dimension(330,40));
		criteriu.setFont(new Font("Arial", Font.ITALIC, 30));
		criteriu.setBackground(new Color(51, 51, 255));
		criteriu.setForeground(Color.white);
		panel.add(criteriu);
		
		filtru = new JTextField("Parametru filtrare");
		filtru.setOpaque(false);
		filtru.setPreferredSize(new Dimension(330,40));
		filtru.setFont(new Font("Arial", Font.BOLD, 30));
		filtru.setForeground(new Color(51, 51, 255));
		panel.add(filtru);
		
		JButton filtreaza = new JButton("Fitrare clienti");
		filtreaza.setPreferredSize(new Dimension(330,40));
		filtreaza.setFont(new Font("Arial", Font.BOLD, 30));
		filtreaza.setForeground(new Color(51, 51, 255));
		filtreaza.addActionListener(new Filtreaza());
		panel.add(filtreaza);
		
		
		JPanel panel2 =new  JPanel();
		panel2.setPreferredSize(new Dimension(1000,100));
		panel2.setOpaque(false);
		
		JButton statistici = new JButton("Statistici clienti");
		statistici.setPreferredSize(new Dimension(330,40));
		statistici.setFont(new Font("Arial", Font.BOLD, 30));
		statistici.setForeground(new Color(51, 51, 255));
		statistici.addActionListener(new Statistici());
		panel2.add(statistici);
		panel.add(panel2);
		
		program=new JTextField("Program consultatii");
		program.setOpaque(false);
		program.setPreferredSize(new Dimension(500,40));
		program.setFont(new Font("Arial", Font.BOLD, 30));
		program.setForeground(new Color(51, 51, 255));
		panel.add(program);
		
		JButton schimbaProgram = new JButton("Actualizare program");
		schimbaProgram.setPreferredSize(new Dimension(500,40));
		schimbaProgram.setFont(new Font("Arial", Font.BOLD, 30));
		schimbaProgram.setForeground(new Color(51, 51, 255));
		schimbaProgram.addActionListener(new Program());
		panel.add(schimbaProgram);
		
		
	    over.add(panel);
	    over.add(imagen1);
	    add(over);
	}
	
	public class Vizualizare implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			updateTabele();
		}

	}
	
	public class Statistici implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new StatisticiGui(id);
		}

	}

	
	public class SetDieta implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String dietaS = (String) dieta.getSelectedItem();
			try
			{
			int row = tabel1.getSelectedRow();
			int id=(int) tabel1.getModel().getValueAt(row, 0);
			DieteticianController.dieta(dietaS,id);
			}
			catch(Exception NullPointerException)
			{
				new MesajGui("Selectati un client pentru alegerea dietei!");
			}
			updateTabele();
		}

	}
	
	public class cautaNume implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if(!name.getText().isEmpty() && !name.getText().contentEquals("Nume Client"))
			{
				String nume =name.getText();
				String col1[] = {"Id","Nume","Varsta","Greutate","Dieta","Telefon"};
			    DefaultTableModel t2 = new DefaultTableModel(col1,0);
			    DieteticianController.cautaNume(nume,id,t2);
			    tabel1.setModel(t2);
			}
			else
			{
				new MesajGui("Introduceti numele clientului!");
			}

		}

	}

	public class Filtreaza implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String param = null;
			if((filtru.getText().contentEquals("Parametru filtrare") || filtru.getText().isEmpty() )&&criteriu.getSelectedItem().equals("Dieta"))
			{
				param = (String) dieta.getSelectedItem();
			
			}
			if((!filtru.getText().contentEquals("Parametru filtrare") && !filtru.getText().isEmpty()) || param!=null)
			{
			if(param==null)
			{
					param=filtru.getText();
			}
			String col1[] = {"Id","Nume","Varsta","Greutate","Dieta","Telefon"};
			DefaultTableModel t = new DefaultTableModel(col1,0);
			DieteticianController.filtrare((String)criteriu.getSelectedItem(), param,t,id);
			tabel1.setModel(t);
			}
			else
			{
				new MesajGui("Introduceti field-ul pentru filtrare!");
			}
		}

	}
	
	public class Program implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(!program.getText().isEmpty() && !program.getText().contentEquals("Program consultatii"))
			{
				
			}
			else
			{
				new MesajGui("introduceti field-ul de program!");
			}
		}

	}


}
