package controller;

import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

import client.ComenziMonitor;
import model.Dietetician;
import model.User;
import view.AdministratorGui;

public class AdministratorController {

	public static void tabel(String tip,DefaultTableModel t)
	{
		ComenziMonitor.setComanda(tip);
		
		boolean ok=true;
		
		while(ok)
		{
			switch(ComenziMonitor.getComanda())
			{
			case "secretari":
				ArrayList<Object> tabel =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel.size();i+=4)
				{
					Object[] row= {(int) tabel.get(i),(String) tabel.get(i+1),(String) tabel.get(i+2),(String) tabel.get(i+3)};
					t.addRow(row);
				}
				ok=false;
				break;
			case "dieteticieni":
				ArrayList<Object> tabel1 =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel1.size();i+=5)
				{
					Object[] row= {(int) tabel1.get(i),(String) tabel1.get(i+1),(String) tabel1.get(i+2),(String) tabel1.get(i+3),(String) tabel1.get(i+4)};
					t.addRow(row);
				}
				ok=false;
				break;
			}
		}
		
	}
	
	public static void sterge(String tip,String id)
	{
		switch(tip)
		{
		case "sterge secretar":
			ArrayList<Object> lid = new ArrayList<Object>();
			lid.add(id);
			ComenziMonitor.setObiecte(lid);
			ComenziMonitor.setComanda("sterge secretar");
			
			break;
		case "sterge dietetician":
			ArrayList<Object> lid1 = new ArrayList<Object>();
			lid1.add(id);
			ComenziMonitor.setObiecte(lid1);
			ComenziMonitor.setComanda("sterge dietetician");
			
			break;
		default:
			break;
		}
		
		boolean ok=true;
		while(ok)
		{
			if(ComenziMonitor.getComanda().equalsIgnoreCase("sters"))
			{
				AdministratorGui.updateTabele();
				ok=false;
			}
		}
	}
	
	public static void updateSecretar(int id,String nume,String mail,String telefon)
	{
		ArrayList<Object> data = new ArrayList<Object>() {{add(id);add(nume);add(mail);add(telefon);}};
		ComenziMonitor.setObiecte(data);
		ComenziMonitor.setComanda("update secretar");
		
		boolean ok=true;
		while(ok)
		{
			if(ComenziMonitor.getComanda().equalsIgnoreCase("updatat"))
			{
				AdministratorGui.updateTabele();
				ok=false;
			}
		}
	}
	
	public static void updateDietetician(int id,String nume,String mail,String telefon,String program)
	{
		ArrayList<Object> data = new ArrayList<Object>() {{add(id);add(nume);add(mail);add(telefon);add(program);}};
		ComenziMonitor.setObiecte(data);
		ComenziMonitor.setComanda("update dietetician");
		
		boolean ok=true;
		while(ok)
		{
			if(ComenziMonitor.getComanda().equalsIgnoreCase("updatat"))
			{
				AdministratorGui.updateTabele();
				ok=false;
			}
		}
	}
}
