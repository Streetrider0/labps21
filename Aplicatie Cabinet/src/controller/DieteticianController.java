package controller;

import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import client.ComenziMonitor;
import view.DieteticianGui;
import view.MesajGui;

public class DieteticianController {

	
	public static void tabel(String tip,DefaultTableModel t,int id)
	{
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(id);
		ComenziMonitor.setObiecte(list);
		ComenziMonitor.setComanda("filtrareDupa"+tip);
		
		boolean ok=true;
		
		while(ok)
		{
			switch(ComenziMonitor.getComanda())
			{
			case "filtrat":
				switch(tip)
				{
				case "Dietetician":
				{
				ArrayList<Object> tabel1 =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel1.size();i+=6)
				{

					Object[] row= {(int) tabel1.get(i),(String) tabel1.get(i+1),(int) tabel1.get(i+2),(int) tabel1.get(i+3),(String) tabel1.get(i+4),(String) tabel1.get(i+5)};
					t.addRow(row);
				}
				break;
				}
				
				case"DieteticianProgramari":
				{
				ArrayList<Object> tabel2 =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel2.size();i+=4)
				{
					String data_ora=(String) tabel2.get(i+3);
					Object[] row= {(int) tabel2.get(i),(int) tabel2.get(i+1),(int) tabel2.get(i+2),data_ora};
					t.addRow(row);
				}
				break;
				}
				
				}
				ok=false;
				break;
			case "nefiltrat":
				ok=false;
				break;
			}
		}
	}
	
	public static void dieta(String dietaS,int id) {
		// TODO Auto-generated method stub
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(dietaS);
		list.add(id);
		ComenziMonitor.setObiecte(list);
		ComenziMonitor.setComanda("setare dieta");
		
		boolean ok=true;
		while(ok)
		{
			if(ComenziMonitor.getComanda().contentEquals("updatat"))
			{
				DieteticianGui.updateTabele();
				ok=false;
			}
		}
	}
	
	public static void cautaNume(String nume,int dietetician,DefaultTableModel t)
	{
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(nume);
		params.add(dietetician);
		ComenziMonitor.setObiecte(params);
		ComenziMonitor.setComanda("cautare nume dietetician");
		
		boolean ok=true;
		
		while(ok)
		{
			switch (ComenziMonitor.getComanda()) 
			{
				case "gasit":
					ArrayList<Object> client = ComenziMonitor.getObiecte();
					Object[] row= {(int) client.get(0),(String) client.get(1),(int) client.get(2),(int) client.get(3),(String) client.get(4),(String) client.get(5)};
					t.addRow(row);
					ok=false;
					break;
				case "negasit":
					new MesajGui("Client negasit!");
					ok=false;
					break;
			}
		}
	}

	public static void filtrare(String selectedItem, String param, DefaultTableModel t,int id) {
		// TODO Auto-generated method stub
		
		ArrayList<Object> send = new ArrayList<Object>();
		send.add(id);
		send.add(param);
		ComenziMonitor.setObiecte(send);
		
		switch(selectedItem)
		{
		case "Dieta":
			ComenziMonitor.setComanda("filtrare in functie de dieta");
			break;
		case "Varsta":
			ComenziMonitor.setComanda("filtrare in functie de varsta");
			break;
		case "Greutate":
			ComenziMonitor.setComanda("filtrare in functie de greutate");
			break;
		}
		
		boolean ok=true;
		
		while(ok)
		{
			switch(ComenziMonitor.getComanda())
			{
			
			case "filtrat":
				ArrayList<Object> tabel2 =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel2.size();i+=6)
				{
					Object[] row= {(int) tabel2.get(i),(String) tabel2.get(i+1),(int) tabel2.get(i+2),(int) tabel2.get(i+3),(String) tabel2.get(i+4),(String) tabel2.get(i+5)};
					t.addRow(row);
				}
				ok=false;
				break;
			case "nefiltrat":
				new MesajGui("Nu exista clienti care sa indeplineasca filtrarea!");
				ok=false;
				break;
			}
		}
	}

	public static void program(String text, int id) {
		
	}
}
