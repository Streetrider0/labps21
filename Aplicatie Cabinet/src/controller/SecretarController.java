package controller;

import java.awt.Desktop;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.DefaultTableModel;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import client.ComenziMonitor;
import model.Clienti;
import model.Dietetician;
import model.Programare;
import model.User;
import view.MesajGui;
import view.SecretarGui;

public class SecretarController {

	public static void tabel(String tip,DefaultTableModel t)
	{
		ComenziMonitor.setComanda(tip);
		
		boolean ok=true;
		
		while(ok)
		{
			switch(ComenziMonitor.getComanda())
			{
			case "dieteticieni":
				ArrayList<Object> tabel =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel.size();i+=5)
				{
					Object[] row= {(int) tabel.get(i),(String) tabel.get(i+1),(String) tabel.get(i+2),(String) tabel.get(i+3),(String) tabel.get(i+4)};
					t.addRow(row);
				}
				ok=false;
				break;
			case "programari":
				ArrayList<Object> tabel1 =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel1.size();i+=4)
				{
					String data_ora=(String) tabel1.get(i+3);
					Object[] row= {(int) tabel1.get(i),(int) tabel1.get(i+1),(int) tabel1.get(i+2),data_ora};
					t.addRow(row);
				}
				ok=false;
				break;
			case "clienti":
				ArrayList<Object> tabel2 =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel2.size();i+=6)
				{

					Object[] row= {(int) tabel2.get(i),(String) tabel2.get(i+1),(int) tabel2.get(i+2),(int) tabel2.get(i+3),(String) tabel2.get(i+4),(String) tabel2.get(i+5)};
					t.addRow(row);
				}
				ok=false;
				break;
			case "numeclient":
				break;
			}
		}
	}
	
	public static void cautaNume(String nume,DefaultTableModel t)
	{
		ComenziMonitor.setObiecte(new ArrayList<Object>() {{add(nume);}});
		ComenziMonitor.setComanda("cautare nume");
		
		boolean ok=true;
		
		while(ok)
		{
			switch (ComenziMonitor.getComanda()) 
			{
				case "gasit":
					ArrayList<Object> client = ComenziMonitor.getObiecte();
					Object[] row= {(int) client.get(0),(String) client.get(1),(int) client.get(2),(int) client.get(3),(String) client.get(4),(String) client.get(5)};
					t.addRow(row);
					ok=false;
					break;
				case "negasit":
					new MesajGui("Client negasit!");
					ok=false;
					break;
			}
		}

	}
	
	public static void insertClient(String numeTxt,int varstaTxt,int greutateTxt,String telefonTxt)
	{
		ComenziMonitor.setObiecte(new ArrayList<Object>() {{add(numeTxt);add(varstaTxt);add(greutateTxt);add(telefonTxt);}});
		ComenziMonitor.setComanda("insert client");
		
		boolean ok=true;
		
		while(ok)
		{
			if(ComenziMonitor.getComanda().contentEquals("inserat")) {
				ok=false;
				SecretarGui.updateTabele();
			}
		}
	}
	
	public static void stergeClient(int id)
	{
		ComenziMonitor.setObiecte(new ArrayList<Object>() {{add(id);}});
		ComenziMonitor.setComanda("sterge client");
		
        boolean ok=true;
		
		while(ok)
		{
			if(ComenziMonitor.getComanda().contentEquals("sters")) {
				ok=false;
				SecretarGui.updateTabele();
			}
		}
	}
	
	public static void editClient(int id,String nume,int varsta,int greutate,String dieta,String telefon)
	{
		ComenziMonitor.setObiecte(new ArrayList<Object>() {{add(id);add(nume);add(varsta);add(greutate);add(dieta);add(telefon);}});
		ComenziMonitor.setComanda("edit client");
		
        boolean ok=true;
		
		while(ok)
		{
			if(ComenziMonitor.getComanda().contentEquals("updatat")) {
				ok=false;
				SecretarGui.updateTabele();
			}
		}
	}

	public static void programeaza(int dietet,int client, String data)
	{
		ArrayList<Object> progr = new ArrayList<Object>();
		progr.add(dietet);
		progr.add(client);
		progr.add(data);
		ComenziMonitor.setObiecte(progr);
		ComenziMonitor.setComanda("programeaza");
		
		boolean ok=true;
		
		while(ok)
		{
			switch(ComenziMonitor.getComanda())
			{
			case "programat":
				ok=false;
				SecretarGui.updateTabele();
				new MesajGui("Programare realizata!");
				break;
			case "esuat":
				ok=false;
				new MesajGui("Programare esuata!");
				break;
			}
		}
	}
	
	public static void stergeProgramare(int id)
	{
		ComenziMonitor.setObiecte(new ArrayList<Object>() {{add(id);}});
		ComenziMonitor.setComanda("stergeProgramare");
		
		boolean ok=true;
		
		while(ok)
		{
			if(ComenziMonitor.getComanda().contentEquals("sters"))
			{
				ok=false;
				SecretarGui.updateTabele();
			}
		}
	}

	public static void rapoarte() throws IOException {
		// TODO Auto-generated method stub
		ComenziMonitor.setComanda("rapoarte");
		
		boolean ok=true;
		
		while(ok)
		{
			if(ComenziMonitor.getComanda().contentEquals("clienti"))
			{
				ArrayList<Clienti> clienti = new ArrayList<Clienti>();
				ok=false;
				for(Object o:ComenziMonitor.getObiecte())
				{
					clienti.add((Clienti) o);
				}
				Desktop desktop = Desktop.getDesktop();  
				
				
				File csvFile = new File("clienti.csv");
				try (PrintWriter csvWriter = new PrintWriter(new FileWriter(csvFile));){
				  for(Clienti c : clienti){
				    csvWriter.println(c.toString());
				  }
				} catch (IOException e) {
				    //Handle exception
				    e.printStackTrace();
				}

				 JSONArray clienti1 = new JSONArray(clienti);
			     try (FileWriter file = new FileWriter("clienti.json")) 
			     {
			 
			            file.write((clienti1).toString());
			            file.flush();
			     }
			     catch (IOException e) {
			    	 e.printStackTrace();
			     }
				
			    File f = new File("clienti.json");
				desktop.open(csvFile);
				desktop.open(f);
				
			}
		}
	}
	
	public static void filtrare(String tip,String parametru,DefaultTableModel t)
	{
		ComenziMonitor.setObiecte(new ArrayList<Object>() {{add(parametru);}});
		ComenziMonitor.setComanda("filtrareDupa"+tip);
		
		boolean ok=true;
		
		while(ok)
		{
			switch(ComenziMonitor.getComanda())
			{
			case "filtrat":
				ArrayList<Object> tabel2 =ComenziMonitor.getObiecte();
				for(int i=0;i<tabel2.size();i+=6)
				{
					Object[] row= {(int) tabel2.get(i),(String) tabel2.get(i+1),(int) tabel2.get(i+2),(int) tabel2.get(i+3),(String) tabel2.get(i+4),(String) tabel2.get(i+5)};
					t.addRow(row);
				}
				ok=false;
				break;
			case "nefiltrat":
				new MesajGui("Nu exista clienti care sa indeplineasca filtrul!");
				ok=false;
				break;
			}
		}
	}
}
