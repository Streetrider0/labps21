package controller;

import java.util.ArrayList;


import client.ComenziMonitor;
import model.User;
import view.*;

public class LoginController {

	public LoginController() {};
	
	public static void loginRequest(String email, String parola)
	{
		ArrayList<Object> user = new ArrayList<Object>();
		user.add(new User(0,null,email,parola,null));
		ComenziMonitor.setObiecte(user);
		ComenziMonitor.setComanda("verifica user");
		
		boolean ok=true;
		while(ok)
		{
			switch(ComenziMonitor.getComanda())
			{
			case "inexistent":
				new MesajGui("Utilizator inexistent!");
				ok=false;
				break;
			
			case "class model.Administrator":
				new AdministratorGui(((User) ComenziMonitor.getObiecte().get(0)).getNume(),((User) ComenziMonitor.getObiecte().get(0)).getEmail());
				ok=false;
				break;
				
			case "class model.Dietetician":
				new DieteticianGui(((User) ComenziMonitor.getObiecte().get(0)).getNume(),((User) ComenziMonitor.getObiecte().get(0)).getEmail(),((User) ComenziMonitor.getObiecte().get(0)).getId());
				ok=false;
				break;
				
			case "class model.Secretar":
				new SecretarGui(((User) ComenziMonitor.getObiecte().get(0)).getNume(),((User) ComenziMonitor.getObiecte().get(0)).getEmail());
				ok=false;
				break;
		     }
	   }
}
}
