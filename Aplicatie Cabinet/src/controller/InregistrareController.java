package controller;

import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;

import client.ComenziMonitor;
import model.*;


public class InregistrareController {
	
	private static Object o;
	
	public static void register(String tip,String nume,String mail,String parola,String tel,JFrame f)
	{
		ArrayList<Object> obiecte =new ArrayList<Object>();
		obiecte.add(tip);
		obiecte.add(nume);
		obiecte.add(mail);
		obiecte.add(parola);
		obiecte.add(tel); 
		ComenziMonitor.setObiecte(obiecte);
		ComenziMonitor.setComanda("inregistrare");
	
	boolean ok=true;
	while(ok)
	{
		if(ComenziMonitor.getComanda().equalsIgnoreCase("cont creat"))
		{
			ok=false;
			f.dispatchEvent(new WindowEvent(f, WindowEvent.WINDOW_CLOSING));
		}
			
	}
}
}
