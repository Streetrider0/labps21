-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: tema2
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrator` (
  `idadministrator` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `parola` varchar(45) DEFAULT NULL,
  `telefon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idadministrator`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (1,'Pavel','pavel@gmail.com','admin',NULL),(2,'adasd asda','asdasdsad','asdasdasdas','dasdasdasd'),(3,'Pavel Alexandru','alexpavel1998@gmail.com','alexpavel229','0725513235'),(4,'admin test','admin','admin','admin');
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clienti`
--

DROP TABLE IF EXISTS `clienti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clienti` (
  `idclient` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(45) DEFAULT NULL,
  `varsta` int(11) DEFAULT NULL,
  `greutate` int(11) DEFAULT NULL,
  `dieta` varchar(45) DEFAULT NULL,
  `telefon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idclient`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clienti`
--

LOCK TABLES `clienti` WRITE;
/*!40000 ALTER TABLE `clienti` DISABLE KEYS */;
INSERT INTO `clienti` VALUES (5,'Dan Andrei',21,79,'Rina','0786342562'),(7,'Simon Adriana',19,56,'Paleo','0786452312'),(10,'Mihail Alexandru',34,72,'Montignac','0789563423'),(12,'Pavel Vlad',32,89,NULL,'0789563423');
/*!40000 ALTER TABLE `clienti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dietetician`
--

DROP TABLE IF EXISTS `dietetician`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dietetician` (
  `iddietetician` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `parola` varchar(45) DEFAULT NULL,
  `program` varchar(45) DEFAULT NULL,
  `telefon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iddietetician`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dietetician`
--

LOCK TABLES `dietetician` WRITE;
/*!40000 ALTER TABLE `dietetician` DISABLE KEYS */;
INSERT INTO `dietetician` VALUES (17,'Liviu Daraban','drliviu@gmail.com','mihailiviu','09:00-17:00','0789452367'),(20,'Test  Dietetician','dietetician','dietetician','12:00-18:00','dietetician'),(22,'Gherman Marian','ghermanmarian@yahoo.com','drgherman',NULL,'0789634251');
/*!40000 ALTER TABLE `dietetician` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programari`
--

DROP TABLE IF EXISTS `programari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programari` (
  `idprogramari` int(11) NOT NULL AUTO_INCREMENT,
  `iddietetician` int(11) DEFAULT NULL,
  `idClient` int(11) DEFAULT NULL,
  `dataSiOra` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idprogramari`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programari`
--

LOCK TABLES `programari` WRITE;
/*!40000 ALTER TABLE `programari` DISABLE KEYS */;
INSERT INTO `programari` VALUES (22,17,7,'13:00 ; 22/06/2020'),(23,17,7,'16:00 ; 27/07/2020'),(24,17,12,'13:00 ; 27/07/2020'),(25,17,10,'16:00 ; 23/08/2020'),(26,20,10,'13:00 ; 23/06/2020'),(29,20,7,'12:00 ; 22/06/2020'),(30,17,7,'11:00 ; 22/06/2020');
/*!40000 ALTER TABLE `programari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secretar`
--

DROP TABLE IF EXISTS `secretar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secretar` (
  `idsecretar` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `parola` varchar(45) DEFAULT NULL,
  `telefon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idsecretar`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secretar`
--

LOCK TABLES `secretar` WRITE;
/*!40000 ALTER TABLE `secretar` DISABLE KEYS */;
INSERT INTO `secretar` VALUES (29,'Test 1','secretar','secretar','secretar'),(30,'Marin Ioana','marinioana@yahoo.com','marinioana','0785436231'),(31,'Vasile Dan','vasile89@yahoo.com','vasile8989','0783456231'),(32,'sadas asdas','asdas','adasd','sadas');
/*!40000 ALTER TABLE `secretar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tema2'
--

--
-- Dumping routines for database 'tema2'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-15 21:12:37
